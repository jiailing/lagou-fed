import React from 'react'
import Header from '../components/Header'
import {graphql} from 'gatsby'
import SEO from '../components/SEO'

export default function List ({data}) {
  console.log(data)
  const {allMarkdownRemark: {nodes}} = data
  return (
    <div>
    <SEO title="List Page " description="list page description"/>
      <Header/>
      <h1>List page</h1>
      <ul>
        {nodes.map(node => (
          <div key={node.id}>
            <p>title: {node.frontmatter.title}</p>
            <p>date: {node.frontmatter.date}</p>
            <div dangerouslySetInnerHTML={{ __html: node.html }}></div>
          </div>
        ))}
      </ul>
    </div>
  )
}

export const query = graphql`
query {
  allMarkdownRemark {
    nodes {
      id
      html
      frontmatter {
        title
        date
      }
      fileAbsolutePath
      internal {
        type
      }
    }
  }
}


`