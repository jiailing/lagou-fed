import React from "react"
import {Link, graphql} from 'gatsby'
import SEO from '../components/SEO'
import styles from '../styles/index.module.css'

export default function Home({data}) {
  console.log(data)
  return (
    <>
      <SEO title="Index Page" />
      <div>
        <Link class={styles.red} to="/person/zhangsan">张三</Link>
        <Link to="/person/lisi">李四</Link>
        <p>{data.site.siteMetadata.title}</p>
        <p>{data.site.siteMetadata.auth}</p>
      </div>
    </>
  )
}

export const query = graphql`
  query MyQuery {
    site {
      siteMetadata {
        title
        auth
      }
    }
  }
`