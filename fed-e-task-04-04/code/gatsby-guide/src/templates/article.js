import { graphql } from 'gatsby'
import React from 'react'

export default function Article({data}) {
  console.log(data)
  const {markdownRemark} = data
  return (
    <div>
      <p>{markdownRemark.frontmatter.title}</p>
      <p>{markdownRemark.frontmatter.date}</p>
      <p dangerouslySetInnerHTML={{__html: markdownRemark.html}}></p>
    </div>
  )
}

export const query = graphql`
query ($slug: String) {
  markdownRemark(fields: {slug: {eq: $slug}}) {
    id
    html
    frontmatter {
      title
      date
    }
  }
}
`