import React from 'react'

export default function Person({pageContext}) {
  const { name, age } = pageContext
  return (
    <div>
      Person {name} {age}
    </div>
  )
}
