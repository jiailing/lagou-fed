import React from 'react'
import { graphql, useStaticQuery } from "gatsby";
export default function Header() {
  const data = useStaticQuery(graphql`
    query NonPageQuery {
      site {
        siteMetadata {
          title
          auth
        }
      }
    }
  `)
  return (
    <div>
      <p>{data.site.siteMetadata.title}</p>
      <p>{data.site.siteMetadata.auth}</p>
    </div>
  )
}
