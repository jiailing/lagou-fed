/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */

module.exports = {
  siteMetadata: {
    title: 'hello gatsby',
    auth: 'Cathy'
  },
  /* Your site config here */
  plugins: [
    {
      // 将本地源信息添加到数据层
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'json',
        path: `${__dirname}/json/`
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: 'markdown',
        path: `${__dirname}/src/posts`
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: 'xml',
        path: `${__dirname}/xml/`
      }
    },
    'gatsby-transformer-remark',
    'gatsby-transformer-json',
    // 'gatsby-plugin-sharp',
    // 'gatsby-transformer-sharp',
    // {
    //   resolve: 'gatsby-source-strapi',
    //   options: {
    //     apiURL: 'http://localhost:1337',
    //     contentTypes: [`posts`]
    //   }
    // }
    {
      resolve: 'gatsby-source-mystrapi',
      options: {
        apiURL: 'http://localhost:1337',
        contentTypes: ['Post', 'Product']
      }
    },
    "gatsby-transformer-xml",
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-less"
  ],
}
