const { parseString } = require('xml2js')
const { promisify } = require('util')
const parse = promisify(parseString)
const createNodeHelpers = require('gatsby-node-helpers').default

async function onCreateNode({ node, loadNodeContent, actions }) {
  const { createNode } = actions
  if (node.internal.mediaType === 'application/xml') {// 判断 node 是否是我们需要转换的节点
    let content = await loadNodeContent(node)
    let obj = await parse(content, {explicitArray: false, explicitRoot: false})
    console.log('xml obj', obj)
    console.log('xml test', content)
    const {createNodeFactory} = createNodeHelpers({
      typePrefix: 'XML'
    })
    const createNodeObject = createNodeFactory('parsedContent')
    createNode(createNodeObject(obj))
  }
}

module.exports = {
  onCreateNode,
}