import Head from 'next/head'
import { Box, Heading, Flex, Image, Input, HStack, Button, Text } from '@chakra-ui/react'
import styles from '../styles/Home.module.css'
import {css} from '@emotion/react'
import {BiMicrophone} from 'react-icons/bi'
import {AiOutlineSearch} from 'react-icons/ai'
import Link from 'next/link'

const InputWrap = css`
    display: flex;
    align-items: center;
    box-sizing: border-box;
    box-shadow: 0 1px 6px 0 rgb(32 33 36 / 28%);
    border-radius: 25px;
    height: 40px;
    margin-top: 20px;
    margin-bottom: 20px;
    padding-left: 10px;
}
`

const InputCss = css`
  width: 100%;
  border: none;
  outline: none;
  height: 100%;
  padding-left: 10px;
  font-size: 16px;
`

const ButtonCss = css`
  border: none;
  background: none
`

export default function Home() {
  const data = [
    {name: 'GitHub', path: 'https://github.com'},
    {name: 'GitHub', path: 'https://github.com'},
    {name: 'GitHub', path: 'https://github.com'},
    {name: 'GitHub', path: 'https://github.com'},
  ]
  return (
    <div className={styles.container}>
      <Head>
        {/* <link rel="icon" href="/favicon.ico" /> */}
        <title>新标签页</title>
      </Head>
      <Box maxW="1200px" mx="auto" mt="20px" flexDirection="column">
        <Flex w="500px" flexDir="column" alignItems="stretch">
          <Image src="/google_logo.svg" h="92px" w="272px" alignSelf="center" />
          <Box css={InputWrap}>
            <AiOutlineSearch color="gray.500"/>
            <Input flex={1} css={InputCss} type="search" autoComplete="off" spellCheck="false" aria-live="polite" placeholder="在 Google 上搜索，或者输入一个网址" />
            <Button css={ButtonCss}>
              <Image src="/microphone.svg" />
            </Button>
          </Box>
          <Flex flexWrap>
            {data.map(item => (
              <Box key={item.path} href={item.path} w="100px">
                <Image src={`${item.path}/favicon.ico`}/>
                <Text>{item.name}</Text>
              </Box>
            ))}
          </Flex>
        </Flex>
        </Box>
    </div>
  )
}
