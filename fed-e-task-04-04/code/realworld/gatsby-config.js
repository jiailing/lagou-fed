module.exports = {
  siteMetadata: {
    title: "realworld",
  },
  plugins: [
    "gatsby-plugin-react-helmet",
    {
      resolve: 'gatsby-plugin-create-client-paths',
      options: {
        prefixes: ['/app/*']
      }
    },
    {
      resolve: 'gatsby-source-list',
      options: {
        apiURL: 'https://conduit.productionready.io/api'
      }
    },
    'gatsby-plugin-article', // 顺序很重要，先创建列表，再创建详情页
  ],
};
