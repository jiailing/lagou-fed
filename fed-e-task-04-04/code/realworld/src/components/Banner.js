import React from 'react'
import { useDispatch, useSelector } from "react-redux";

export default function Banner() {
  const dispatch = useDispatch() // 获取到 dispatch 方法
  const counterReducer = useSelector(state => state.counterReducer) // 获取到 store 数据
  return (
    <div className="banner">
      <div className="container">
        <h1 className="logo-font">conduit {counterReducer.count}</h1>
        <p>A place to share your knowledge.</p>
        <button onClick={() => dispatch({type: 'increment'})}>+1</button>
        <button onClick={() => dispatch({type: 'increment_async'})}>+1</button>
      </div>
    </div>
  )
}
