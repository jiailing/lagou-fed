import { navigate } from 'gatsby'
import React from 'react'
import useLogin from '../hooks/useLogin'

export default function PrivateRoute({component: Component, ...rest}) {
  const [isLogin, loading] = useLogin()
  if (loading) return null
  if (isLogin) return <Component {...rest}/>
  navigate('/login')
  return null
}
