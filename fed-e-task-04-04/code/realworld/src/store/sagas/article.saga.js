import axios from 'axios'
import { takeEvery, put } from 'redux-saga/effects'

function* loadArticles (action) {
  let { data } = yield axios.get('/articles', {
    params: action.payload
  })
  yield put({type: 'loadArticlesSuccess', payload: data.articles })
}

export default function* articleSaga () {
  yield takeEvery('loadArticles', loadArticles)
}