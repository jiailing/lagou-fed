import {all} from 'redux-saga/effects'
import articleSaga from './article.saga'
import authSaga from './auth.saga'
import counterSaga from './counter.saga'

export default function* rootSaga () {
  yield all([counterSaga(), authSaga(), articleSaga()])
}
