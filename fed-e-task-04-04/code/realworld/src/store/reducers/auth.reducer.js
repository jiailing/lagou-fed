const initialState = {}

export default function authReducer (state = initialState, action) {
  switch (action.type) {
    case 'loginSuccess':
    case 'loadUserSuccess':
      return {
        success: true,
        user: action.payload
      }
    case 'loginFailed':
    case 'loadUserFailed':
      return {
        success: false,
        errors: action.payload
      }
    default:
      return state
  }
}