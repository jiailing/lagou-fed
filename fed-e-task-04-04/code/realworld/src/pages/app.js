import React from 'react'
import { Router } from '@reach/router'
import Settings from '../components/settings'
import Create from '../components/create'
import PrivateRoute from '../components/PrivateRoute'

export default function App() {
  return (
    <Router>
      <PrivateRoute path="/app/settings"  component={Settings}/>
      <PrivateRoute path="/app/create"  component={Create}/>
    </Router>
  )
}
