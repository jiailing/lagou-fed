const React = require('react')
const {Provider} = require('react-redux')
const Layout = require('./src/components/Layout').default

const createStore = require('./src/store/createStore').default

const axios = require('axios')
axios.defaults.baseURL = 'https://conduit.productionready.io/api'

exports.wrapPageElement = ({element}) => <Layout>{element}</Layout>

exports.wrapRootElement = ({element}) => <Provider store={createStore()}>{element}</Provider>