import Head from "next/head";
import style from './list.module.css'
import { readFile } from "fs";
import { promisify } from "util";
import { join } from "path";

const read = promisify(readFile)

export default function List ({data}) {
  return (
    <>
      <Head>
        <title>List Page</title>
      </Head>
      <div className={style.demo}>List Page</div>
      <div>{data}</div>
    </>
  )
}

export async function getServerSideProps () {
  let data = await read(join(process.cwd(), 'pages', '_app.js'), 'utf-8')
  console.log('Hello')
  return {
    props: {
      data
    }
  }
}