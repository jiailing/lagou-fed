import Link from 'next/link'
import Head from 'next/head'

export default function Home() {
  return <>
    <Head>
      <title>Index Page</title>
    </Head>
    <div>
      Index Page works
      <Link href="/list"><a className="demo">Jump to List Page</a></Link>
      <img src="/images/1.jpeg" height="100" />
    </div>
    <style jsx>{`
      .demo {
        color: red
      }
    `}</style>
  </>
}
