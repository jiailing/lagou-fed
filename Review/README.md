# 复习

## 01-01

1. Object.assign()是深拷贝还是浅拷贝
浅拷贝。

2. 对象无法使用for...of，需要实现迭代器接口
```js
// obj 实现可迭代接口 Iterable
const obj = {
  // iterator 方法
  [Symbol.iterator]: function () {
    // 迭代器接口 iterator 
    return {
      // 必须要有next方法
      next: function () {
        // 迭代结果接口 IterationResult
        return {
          value: 1,
          done: true
        }
      }
    }
  }
}
```

3. Generator函数：Generator函数实现迭代器Iterator
```js
const todos = {
  life: ['吃饭', '睡觉', '打豆豆'],
  learn: ['语文', '数学', '英语'],
  work: ['喝茶'],

  // 实现迭代器接口
  [Symbol.iterator]: function * () {
    const all = [...this.life, ...this.learn, ...this.work]
    for (const item of all) {
      yield item
    }
  }
}

for(const item of todos) {
  console.log(item)
}
```

3. 封装了一个生成器函数执行器
```js
function co(main) {
  // 调用生成器函数得到一个生成器对象
  const generator = main()

  // 递归实现generator.next()的调用，直到done为true终止
  function handleResult(result) {
    if(result.done) return
    result.value.then(data=>{
      console.log(data)
      handleResult(generator.next(data))
    }, error => {
      g.throw(error)
    })
  }

  handleResult(generator.next())
}

co(main)
```

## 01-02

1. 模拟实现柯里化

```js
function getSum (a, b, c) {
  return a + b + c
}

const myCurried = curry(getSum)
console.log(myCurried(1, 2, 3)) // 6
console.log(myCurried(1)(2, 3))// 6
console.log(myCurried(1, 2)(3))// 6

function curry(fn) {
  return function curriedFn (...args) {
    if(args.length < fn.length) {
      return function () {
        return curriedFn(...args, ...arguments)
      }
    } else {
      return fn(...args)
    }
  }
}
```

2. 模拟实现flowRight

```js
// function compose (...args) {
//   return function (value) {
//     return args.reverse().reduce(function (acc, fn) {
//       return fn(acc)
//     }, value)
//   }
// }

// 将上面的写法修改成箭头函数
const compose = (...args) => value => args.reverse().reduce((acc, fn) => fn(acc), value)

const reverse = arr => arr.reverse()
const first = arr => arr[0]
const toUpper = str => str.toUpperCase()

const f = compose(toUpper, first, reverse)

console.log(f(['one', 'two', 'three'])) // THREE
```