# 2020-05-22直播内容 -- 回顾 & 补充

## Promise 相关

- Promise 应用：
- 执行流程 （宏任务/微任务）
  + 每一个任务（函数）的执行过程都有可能产生宏任务和微任务
  + 每个任务执行的最后，需要先执行完所有的微任务，再开始执行宏任务
- Generator / Async / Await
  - await是yield的语法糖
- Promise.all() 失败问题
  - 如果数组中的任意一个任务失败就会导致整个任务失败
  - 对数组中的每一个promise对象都去处理一下，catch，确保不会出现异常
  - 也可以使用Promise.allSettled()实现



## 重点：关于this

- this 指向什么取决于何？
this的指向取决于调用，而不是定义。 
函数最外层的this是全局，浏览器下是window，node下是global，严格模式是undefined
- 总结：沿着作用域向上找最近的一个function，看这个function最终是怎么执行的。
- 箭头函数和普通函数的区别在于，普通函数会影响this，而箭头函数不会改变this的指向

#### this问题一

```js
function foo () {
  console.log(this) 
}

const obj = {
  foo: function () {
    console.log(this)
  }
}

/*
通过上面两个例子，都无法知道this指向谁。
因为this指向谁取决调用，而不取决于定义。
上面的this所在的函数都没有调用，所以不确定指向谁
*/
```

#### this问题二

```js
function foo () {
  console.log(this) 
}

const obj = {
  foo: function () {
    console.log(this)
  }
}

// 1. 普通调用
foo() // 此时this指向全局（Window/Global）。严格模式指向undefined


// 2. 作为构造函数调用
new foo()  // 此时指向的是foo的实例，一个空对象，类型是foo。 foo {}

// 3. call的方式调用
foo.call(123) // 此时指向call的参数 [Number: 123]

/*
箭头函数跟this没有关系，this只跟function有关系，
想看this指向谁，要看它所在的最近的一个function，这个function是怎样被执行的
*/
```

#### this问题三

```js
const obj1 = {
  foo: function () {
    console.log(this)
  }
}
/*
此时foo方法没调用，不知道this指向谁。我们可以让它指向任意一种，如下：
*/
const fn = obj1.foo
fn() // this指向全局

obj1.foo() // this指向obj1 

new obj1.foo() // this指向foo的实例对象：foo {}

// -----------

const obj2 = {
  foo: function () {
    function bar () {
      console.log(this)
    }
    bar()
  }
}
/*
注意：此时foo方法没调用，不知道this指向谁
*/
obj2.foo() // 此时this指向全局，因为this最近的function是bar，要看bar是怎么被调用的，当然bar是全局调用的

// 箭头函数和普通函数的区别在于，普通函数会影响this，而箭头函数不会改变this的指向
const obj3 = {
  foo: function () {
    const bar = () => {
      console.log(this)
    }
    bar()
  }
}
// 
obj3.foo() // 沿着this所在的作用域往上找，找到最近的function，此时this指向obj3
```



## ES2018+

- ES2018
  - 展开和剩余在对象上的应用

    - const {one, four, ...rest} = obj  rest是剩余键值对组成的对象
    - const obj = {foo: 'bar', ...rest}  使用...展开对象为一个个键值对

  - 正则表达式的增强

    - 指定组的名字：`const reg2 = /(?<year>\d{4})-(?<month>\d{2})-(?<day>\d{2})/`

      ```js
      const reg2 = /(?<year>\d{4})-(?<month>\d{2})-(?<day>\d{2})/
      const r3 = reg2.exec(date)
      console.log(r3) // ['2020-05-20', '2020','05','20', groups: [Object: null prototype] { year: '2020', month: '05', day: '20' }]
      ```

    - 环视

      ```js
      // 环视
      const intro = '张三是张三，张三丰是张三丰，张三不是张三丰，张三丰也不是张三'
      console.log(intro.replace(/张三/g, '李四')) // 李四是李四，李四丰是李四丰，李四不是李四丰，李四丰也不是李四
      
      // 环视，向后断言，向后否定
      console.log(intro.replace(/张三(?!丰)/g, '李四')) // 李四是李四，张三丰是张三丰，李四不是张三丰，张三丰也不是李四
      // 环视，向后断言，向后肯定
      console.log(intro.replace(/张三(?=丰)/g, '李四')) // 张三是张三，李四丰是李四丰，张三不是李四丰，李四丰也不是张三
      
      // 环视，向前断言，向前肯定
      console.log('A00 B00'.replace(/(?<=A)00/g, '88')) // A88 B00
      // 环视，向前断言，向前否定
      console.log('A00 B00'.replace(/(?<!A)00/g, '88')) // A00 B88
      ```

  - Promise.prototype.finally() 无论promise成功失败，finally中的回调函数都会执行
- ES2019
  - 数组稳定排序
  - try...catch 参数可省略，直接catch{...}
- ES2020
  - 空值合并运算符
  - 可选链运算符
  - Promise.allSettled() 不会考虑成功与否，都会返回整体的promise结果
  - BigInt
  - 动态导入



## 关于应用

+ TypeScript

  现阶段的目标是掌握语法层面和运行层面的特性，应用落地必然还是需要结合不同的案例或者项目。



## 面试题

### 1. 阅读下面代码，分析执行结果，并说明具体原因。

```js
console.log('A')

setTimeout(() => {
  console.log('B')
}, 1000);

const start = new Date()

while(new Date() - start < 3000) {}

console.log('C')

setTimeout(() => {
  console.log('D')
}, 0);

new Promise((resole, reject) => {
  console.log('E')
  foo.bar(100)
})
.then(() => console.log('F'))
.then(() => console.log('G'))
.catch(() => console.log('H'))

console.log('I')
```

主任务执行：

A 先在主任务中执行

C while循环3秒结束后执行输出C

E Promise的构造参数中的回调函数内部是同步执行

  注意：foo.bar未定义，后面的then不会执行，而是进入catch

I 在主任务中执行

H 主任务执行完毕后，开始执行微任务

B 微任务执行完后，Event Loop调用消息队列中的宏任务进入主任务执行

D 主任务执行完后，Event Loop调用消息队列中的宏任务进入主任务执行



微任务（本轮主任务末尾执行）：

H 微任务比宏任务先执行



消息队列（宏任务）顺序：

B 因为while循环的3秒定时阻塞了主任务，使得B有机会最先进入消息队列

D 在主任务中分配的定时任务作为宏任务进入消息队列



所以答案是： A C E I H B D



----



```js
setTimeout(() => console.log('A'), 0)
setTimeout(() => console.log('B'), 1000)

Promise.resolve()
.then(() => {
  setTimeout(() => console.log('C'), 0)
  setTimeout(() => console.log('D'), 1000)
  console.log('E')
  Promise.resolve().then(() => console.log('F'))
})
.then(() => console.log('G'))

setTimeout(() => console.log('H'), 0)
setTimeout(() => console.log('I'), 1000)
```

输出顺序：（第一轮主任务中没有输出，所以主任务执行完后，执行第七行then中的微任务）

E // 第一个微任务开始执行，先输出

F // 第一个微任务中的微任务，第二个输出

G // 第二个微任务，第三个输出

A // 主任务中，第一个进入消息队列

H // 主任务中，第2个进入消息队列

C // 微任务中，第3个进入消息队列

B // 主任务中，先结束1s定时器，第4个进入消息队列

I // 主任务中，结束1s定时器，第5个进入消息队列

D // 微任务中，结束1s定时器，第6个进入消息队列

