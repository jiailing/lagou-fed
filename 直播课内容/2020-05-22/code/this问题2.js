function foo () {
  console.log(this) 
}

const obj = {
  foo: function () {
    console.log(this)
  }
}

// 1. 普通调用
foo() // 此时this指向全局（Window/Global）。严格模式指向undefined


// 2. 作为构造函数调用
new foo()  // 此时指向的是foo的实例，一个空对象，类型是foo。 foo {}

// 3. call的方式调用
foo.call(123) // 此时指向call的参数 [Number: 123]

/*
箭头函数跟this没有关系，this只跟function有关系，
想看this指向谁，要看它所在的最近的一个function，这个function是怎样被执行的
*/