var foo = 100

async function main () {
  foo = foo + await Promise.resolve(10) // foo刚开始是100，已经在内存里等待着在了
  // foo = await Promise.resolve(10) + foo //如果这样写，foo最终结果就是111
  console.log('main', foo) // main 110
}

main() // 此处没有await，main是异步函数，所以会在主任务执行完后才会执行

foo++

console.log('global', foo) // global 101