const date = '2020-05-20'

const reg = /(\d{4})-(\d{2})-(\d{2})/

const r = date.match(reg)
console.log(r) // ['2020-05-20', '2020','05','20']

const res = reg.exec(date)
console.log(res) // res 的输出结果同上的r , ['2020-05-20', '2020','05','20']

// 圆括号是第几个出现的，就是第几组

// 现在可以指定组的名字：
const reg2 = /(?<year>\d{4})-(?<month>\d{2})-(?<day>\d{2})/
const r3 = reg2.exec(date)
console.log(r3) // ['2020-05-20', '2020','05','20', groups: [Object: null prototype] { year: '2020', month: '05', day: '20' }]

// 环视
const intro = '张三是张三，张三丰是张三丰，张三不是张三丰，张三丰也不是张三'
console.log(intro.replace(/张三/g, '李四')) // 李四是李四，李四丰是李四丰，李四不是李四丰，李四丰也不是李四

// 环视，向后断言，向后否定
console.log(intro.replace(/张三(?!丰)/g, '李四')) // 李四是李四，张三丰是张三丰，李四不是张三丰，张三丰也不是李四
// 环视，向后断言，向后肯定
console.log(intro.replace(/张三(?=丰)/g, '李四')) // 张三是张三，李四丰是李四丰，张三不是李四丰，李四丰也不是张三

// 环视，向前断言，向前肯定
console.log('A00 B00'.replace(/(?<=A)00/g, '88')) // A88 B00
// 环视，向前断言，向前否定
console.log('A00 B00'.replace(/(?<!A)00/g, '88')) // A00 B88
