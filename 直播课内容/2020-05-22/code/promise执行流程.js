// 1. 执行流程问题

setTimeout(() => console.log('A'), 0)
setTimeout(() => console.log('B'), 1000)

Promise.resolve()
.then(() => {
  setTimeout(() => console.log('C'), 0)
  setTimeout(() => console.log('D'), 1000)
  console.log('E')
  Promise.resolve().then(() => console.log('F'))
})
.then(() => console.log('G'))

setTimeout(() => console.log('H'), 0)
setTimeout(() => console.log('I'), 1000)

/*
输出顺序：（第一轮主任务中没有输出，所以主任务执行完后，执行第七行then中的微任务）
E // 第一个微任务开始执行，先输出
F // 第一个微任务中的微任务，第二个输出
G // 第二个微任务，第三个输出
A // 主任务中，第一个进入消息队列
H // 主任务中，第2个进入消息队列
C // 微任务中，第3个进入消息队列
B // 主任务中，先结束1s定时器，第4个进入消息队列
I // 主任务中，结束1s定时器，第5个进入消息队列
D // 微任务中，结束1s定时器，第6个进入消息队列
*/