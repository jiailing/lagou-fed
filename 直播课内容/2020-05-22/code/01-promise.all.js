const axios = require('axios')
// axios是同构的，也就是说NodeJS和浏览器上的使用效果是一样的
console.log('111')

const urls = [
  "https://api.github.com",
  "https://api.github.com/users",
  "https://sss.xxx.com/ssdd",
]

// const promises = urls.map(item => axios(item))
const promises = urls.map(item => axios(item).catch(e=>({}))) // 对每一个promise异常进行捕获

const p = Promise.all(promises)

// result的顺序是对应promises顺序的结果
p.then(result => {
  console.log(result.length)
})
.catch(error => {
  console.log('error: ',error)
})