function foo () {
  console.log(this) 
}

const obj = {
  foo: function () {
    console.log(this)
  }
}

/*
通过上面两个例子，都无法知道this指向谁。
因为this指向谁取决调用，而不取决于定义。
上面的this所在的函数都没有调用，所以不确定指向谁
*/
