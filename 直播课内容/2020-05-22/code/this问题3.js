const obj1 = {
  foo: function () {
    console.log(this)
  }
}
/*
此时foo方法没调用，不知道this指向谁。我们可以让它指向任意一种，如下：
*/
const fn = obj1.foo
fn() // this指向全局

obj1.foo() // this指向obj1 

new obj1.foo() // this指向foo的实例对象：foo {}

// -----------

const obj2 = {
  foo: function () {
    function bar () {
      console.log(this)
    }
    bar()
  }
}
/*
注意：此时foo方法没调用，不知道this指向谁
*/
obj2.foo() // 此时this指向全局，因为this最近的function是bar，要看bar是怎么被调用的，当然bar是全局调用的

// 箭头函数和普通函数的区别在于，普通函数会影响this，而箭头函数不会改变this的指向
const obj3 = {
  foo: function () {
    const bar = () => {
      console.log(this)
    }
    bar()
  }
}
// 
obj3.foo() // 沿着this所在的作用域往上找，找到最近的function，此时this指向obj3
