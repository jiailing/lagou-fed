const axios = require('axios')

const promises = [
  axios("https://api.github.com"),
  axios("https://api.github.com/users")
]

promises.reduce((prev, current) => {
  return prev.then(() => current)
}, Promise.resolve())