// 考察：JavaScript 异步队列、宏任务/微任务

console.log('A')

setTimeout(() => {
  console.log('B')
}, 1000);

const start = new Date()

while(new Date() - start < 3000) {}

console.log('C')

setTimeout(() => {
  console.log('D')
}, 0);

new Promise((resole, reject) => {
  console.log('E')
  foo.bar(100)
})
.then(() => console.log('F'))
.then(() => console.log('G'))
.catch(() => console.log('H'))

console.log('I')

/*
主任务执行：
A 先在主任务中执行
C while循环3秒结束后执行输出C
E Promise的构造参数中的回调函数内部是同步执行
  注意：foo.bar未定义，后面的then不会执行，而是进入catch
I 在主任务中执行
H 主任务执行完毕后，开始执行微任务
B 微任务执行完后，Event Loop调用消息队列中的宏任务进入主任务执行
D 主任务执行完后，Event Loop调用消息队列中的宏任务进入主任务执行

微任务（本轮主任务末尾执行）：
H 微任务比宏任务先执行

消息队列（宏任务）顺序：
B 因为while循环的3秒定时阻塞了主任务，使得B有机会最先进入消息队列
D 在主任务中分配的定时任务作为宏任务进入消息队列

所以答案是： A C E I H B D
*/
