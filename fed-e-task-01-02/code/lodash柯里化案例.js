// 柯里化案例
// ''.match(/\s+/g)
// ''.match(/\d+/g)

const _ = require('lodash')

const match = _.curry(function (reg, str) {
  return str.match(reg)
})

const haveSpace = match(/\s+/g)

console.log(haveSpace('hello world')) // [ ' ' ]
console.log(haveSpace('hello')) // null

const haveNumber = match(/\d+/g)

console.log(haveNumber('123abc456def789')) // [ '123', '456', '789' ]
console.log(haveNumber('jal')) // null


const filter = _.curry(function (func, arr) {
  return arr.filter(func)
})

console.log(filter(haveSpace, ['hello world', 'Ji Ailing', 'cathy', 'yibo', 'Wang Yibo']))
// [ 'hello world', 'Ji Ailing', 'Wang Yibo' ]

const findSpace = filter(haveSpace)
console.log(findSpace(['hello world', 'Ji Ailing', 'cathy', 'yibo', 'Wang Yibo']))
// [ 'hello world', 'Ji Ailing', 'Wang Yibo' ]