// 模拟常用的高阶函数：map every some

const arr = [1, 2, 3, 4]

// map 
const map = (arr, fn) => {
  let result = []
  for(let item of arr) {
    result.push(fn(item))
  }
  return result
}
console.log(map(arr, val => val * val)) // [ 1, 4, 9, 16 ]

// every
const every = (arr, fn) => {
  for(let item of arr) {
    if(!fn(item))return false
  }
  return true
}
console.log(every(arr, v => v > 0)) // true

// some
const some = (arr, fn) => {
  for(let item of arr) {
    if(fn(item))return true
  }
  return false
}
console.log(some(arr, v => v % 2 == 0)) // true
