const user1 = {age: 11}
const user2 = {age: 2}
const user3 = {age: 121}

const nameList = [user1.age, user2.age, user3.age]

function fn () {
  const num1 = 1 
  const num2 = 2
}
fn()
// fn()执行完后，num1和num2被当做垃圾被回收，而三个对象的age还被nameList这个变量引用着，所以不会被当做垃圾回收