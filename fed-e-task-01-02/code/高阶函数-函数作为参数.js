function forEach (array, fn) {
  for (let i = 0; i < array.length; i++) {
    fn(array[i])
  }
}

function filter (array, fn) {
  const res = []
  for (let i = 0; i < array.length; i++) {
    if(fn(array[i])) {
      res.push(array[i])
    }
  }
  return res
}

const arr = [1, 2, 4, 5, 2]

forEach(arr, console.log)
console.log(filter(arr, function (item) {
  return item % 2 === 0
}))