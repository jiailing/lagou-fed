// _.curry(func)
// const _ = require('lodash')

function getSum (a, b, c) {
  return a + b + c
}

// const curried = _.curry(getSum)
// console.log(curried(1, 2, 3)) // 6
// console.log(curried(1)(2, 3))// 6
// console.log(curried(1, 2)(3))// 6


const myCurried = curry(getSum)
console.log(myCurried(1, 2, 3)) // 6
console.log(myCurried(1)(2, 3))// 6
console.log(myCurried(1, 2)(3))// 6
console.log(myCurried(1)(2)(3))// 6

function curry(fn) {
  return function curriedFn (...args) {
    if(args.length < fn.length) {
      return function () {
        return curriedFn(...args.concat(Array.from(arguments)))
      }
    }else {
      return fn(...args)
    }
    
  }
}
