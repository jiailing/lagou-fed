// 演示 lodash
// first  last toUpper reverse each includes find findIndx
const _ = require('lodash')
const arr = ['jal', 'cathy', 'yibo', 'lucy']

console.log(_.first(arr))
console.log(_.last(arr))

console.log(_.toUpper(_.first(arr)))

console.log(_.reverse(arr))

const r = _.each(arr, (item, index) => {
  console.log(item, index)
})
console.log(r)
