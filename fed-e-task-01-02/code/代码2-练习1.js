const fp = require('lodash/fp')
const { Maybe, Container } = require('./support')

let maybe = Maybe.of([5, 6, 1])
let ex1 = function (x) {
  return maybe.map(fp.map(fp.add(x)))
}
let r = ex1(1)

console.log(r) // Maybe { _value: [ 6, 7, 2 ] }
