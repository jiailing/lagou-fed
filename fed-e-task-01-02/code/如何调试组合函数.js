// NEVER SAY DIE --> never-say-die

const _ = require('lodash')

const split = _.curry((sep, str)=>_.split(str, sep))
// 为什么要调换两个参数的位置？因为要保证函数只有一个参数的函数，那就要通过柯里化实现。
// 而柯里化想要保留一个参数，那就只能保留最后一个参数，所以要把str放到最后

const join = _.curry((sep, arr) => _.join(arr, sep))

const map = _.curry((fn, arr) => _.map(arr, fn))

const log = v => {
  console.log(v)
  return v
}

const trace = _.curry((tag, v) => {
  console.log(tag, v)
  return v
})

// const f = _.flowRight(join('-'), log, _.toLower, split(' ')) // n-e-v-e-r-,-s-a-y-,-d-i-e
// const f = _.flowRight(join('-'), log, split(' '), _.toLower) // never-say-die
const f = _.flowRight(join('-'), trace('map之后'), map(_.toLower), trace('split之后'), split(' ')) // never-say-die
console.log(f('NEVER SAY DIE'))