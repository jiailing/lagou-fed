const fp = require('lodash/fp')
const { Maybe, Container } = require('./support')

let safeProp = fp.curry(function (x, o) {
  return Maybe.of(o[x])
})
let user = {id: 2, name: 'Albert'}

let map = fp.curry(function (fn, functor){
  return functor.map(fn)
})
let ex3 = fp.flowRight(map(fp.first), safeProp('name'))

let r = ex3(user)
console.log(r) // Maybe { _value: 'A' }