class MayBe {
  static of (value) {
    return new MayBe(value)
  }

  constructor(value) {
    this._value = value
  }
  map (fn) {
    return this.isNothing() ? MayBe.of(null) : MayBe.of(fn(this._value))
  }

  isNothing () {
    return this._value === null || this._value === undefined
  }
}

let r = MayBe.of('hello')
.map(x => x.toUpperCase())
console.log(r) // MayBe { _value: 'HELLO' }

let r2 = MayBe.of(null)
.map(x => x.toUpperCase())
console.log(r2) // MayBe { _value: null }

let r3 = MayBe.of('hello world')
.map(x => x.toUpperCase())
.map(x => null)
.map(x => x.split(' '))
console.log(r3) // MayBe { _value: null } 无法知道null是哪里发生的