const fp = require('lodash/fp')
const { Maybe, Container } = require('./support')

let xs = Container.of(['do', 'ray', 'me', 'fa', 'so', 'la', 'ti', 'do'])
let ex2 = function () {
  return xs.map(fp.first)
}
let r = ex2()
console.log(r) // Container { _value: 'do' }