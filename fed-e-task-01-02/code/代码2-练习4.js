const fp = require('lodash/fp')
const { Maybe, Container } = require('./support')

let ex4 = function (n) {
  return Maybe.of(n).map(parseInt)
}
// let r = ex4(null) // Maybe { _value: null }
let r = ex4('12') // Maybe { _value: 12 }
console.log(r)