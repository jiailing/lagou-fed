// 不纯的，函数的返回值依赖外部的变量
let mini = 18
function checkAge (age) {
  return age >= mini
}

// 纯的（有硬编码，后续可以通过柯里化解决）
function checkAge2 (age) {
  let mini = 18
  return age >= mini
}