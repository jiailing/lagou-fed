// 纯函数slice和不纯函数splice

let arr = [1, 2, 3, 4, 5]

console.log(arr.slice(0, 3))
console.log(arr.slice(0, 3))
console.log(arr.slice(0, 3))

console.log(arr.splice(0, 3))
console.log(arr.splice(0, 3))
console.log(arr.splice(0, 3))

function getSum(n1, n2) {
  return n1 + n2
}

console.log(1, 2)
console.log(1, 2)
console.log(1, 2)