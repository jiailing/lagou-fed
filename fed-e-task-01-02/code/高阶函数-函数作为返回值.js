function makeFn () {
  let msg = 'hello function'
  return function () {
    console.log(msg)
  }
}

const fn = makeFn()
fn() // hello function

makeFn()() // hello function

// once 只执行一次的函数，比如说支付情况，无论用户点多少次，这个函数都只执行一次
function once(fn) {
  let done = false
  return function () {
    if(!done) {
      done = true
      fn.apply(this, arguments)
    }
  }
}

let pay = once(function (money) {
  console.log(`支付了${money}元`)
})

pay(1) // 支付了1元
pay(2)
pay(3)
