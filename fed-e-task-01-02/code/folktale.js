// folktale的使用
const { compose, curry } = require('folktale/core/lambda')
const { toUpper, first } = require('lodash/fp')
// curry 第一个参数写上参数的个数
const f = curry(2, (x, y) => x + y)
console.log(f(1, 2)) // 3
console.log(f(1)(2)) // 3

// folktale中的compose相当于lodash中的flowRight
const f2 = compose(toUpper, first)
console.log(f2(['one', 'two'])) // ONE