function test (func) {
  console.log(func())
}

function test2 () {
  var name = 'hello'
  return name
}

test(function () {
  var name = 'world'
  return name
})

test(test2)