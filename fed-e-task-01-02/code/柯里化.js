// 柯里化演示
// function checkAge (age) {
//   let mini = 18
//   return age >= mini
// }

// 柯里化  普通的纯函数
function checkAge (mini, age) {
  return age >= mini
}

console.log(checkAge(18, 20))
console.log(checkAge(18, 24))
console.log(checkAge(22, 24))

// 闭包，高阶函数，函数的柯里化
function saveMini (mini) {
  return function (age) {
    return age >= mini
  }
}

// ES6 写法, 同上
// const saveMini = mini => age => age >= mini

const checkAge18 = saveMini(18)
const checkAge22 = saveMini(22)
console.log(checkAge18(20))
console.log(checkAge18(24))
console.log(checkAge22(24))