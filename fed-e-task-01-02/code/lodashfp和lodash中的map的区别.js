const _ = require('lodash')
const fp = require('lodash/fp')

// lodash中的map中的函数的参数有三个：(item, index, array)
console.log(_.map(['23', '8', '10'], parseInt)) // [ 23, NaN, 2 ]
// parseInt('23', 0, array) 第二个参数是0，则是10进制
// parseInt('8', 1, array) 第二个参数是1，不合法，输出NaN
// parseInt('10', 2, array) 第二个参数是2，表示2进制，输出2

// lodashFp中的map中的函数的参数有1个：(item)
console.log(fp.map(parseInt, ['23', '8', '10'])) // [ 23, 8, 10 ]