const _ = require('lodash')

const f = _.flowRight(_.toUpper, _.first, _.reverse)
console.log(f(['one', 'two', 'three'])) // THREE

const f2 = _.flowRight(_.flowRight(_.toUpper, _.first), _.reverse)
console.log(f2(['one', 'two', 'three'])) // THREE

const f3 = _.flowRight(_.toUpper, _.flowRight(_.first, _.reverse))
console.log(f3(['one', 'two', 'three'])) // THREE