function fn () {
  const obj1 = {}
  const obj2 = {}

  obj1.name = obj2
  obj2.name = obj1
  
  return 'hello'
}

fn() // fn()执行完毕后，虽然无法从根直达obj1 obj2,但是在回收obj1时，发现obj2依赖了obj1，所以obj1无法回收