import React, {Component} from 'react'

import { inject, observer } from 'mobx-react'

@inject('counter')
@observer
class App extends Component {
  render () {
    const { counter } = this.props
    console.log('counter', counter)
    return (
      <div>
        <button onClick={() => counter.decrement(1)}>-</button>
        <span>{counter.count}</span>
        <button onClick={() => counter.increment(1)}>+</button>
      </div>
    );
  }
}

export default App;
