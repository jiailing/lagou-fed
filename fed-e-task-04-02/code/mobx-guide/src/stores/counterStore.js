// 1. 创建 store 对象，存储默认状态 0
// 2. 将 store 对象放在一个全局的组件可以够得到的地方
// 3. 让组件获取 store 对象中的状态，并将状态显示在组件中
import { observable, action, configure } from 'mobx'

// 通过配置强制程序使用 action 函数更改应用程序中的状态
configure({ enforceActions: 'observed' })
class CounterStore {
  @observable count = 666;

  @action increment = (payload) => {
    console.log('increment', payload)
    this.count = this.count + payload;
  }

  @action decrement = (payload) => {
    this.count = this.count - payload;
  }
}

const counter = new CounterStore();

export default counter;