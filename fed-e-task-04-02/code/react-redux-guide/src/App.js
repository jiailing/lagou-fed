import Modal from './components/Modal'
import Counter from './components/Counter'

function App() {
  return (
    <div className="App">
      <Counter />
      <Modal />
    </div>
  );
}

export default App;
