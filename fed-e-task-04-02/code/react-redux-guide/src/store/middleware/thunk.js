const thunk = ({dispatch}) => next => action => {
  // 1. 当前这个中间件函数不关心你想执行什么样的异步操作，只关心你执行的是不是异步操作，
  // 2. 如果你执行的是异步操作，你在触发 action 的时候，给我传递一个函数，如果执行的是同步操作，就传递一个 action 对象，
  // 3. 异步操作代码要写在你传进来的函数中
  // 4. 当这个中间件函数，在调用你传进来的函数时要将 dispatch 方法传递过去
  if (typeof action === 'function') {
    return action(dispatch)
  }
  next(action)
}
export default thunk