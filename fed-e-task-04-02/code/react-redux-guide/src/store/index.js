import { createStore, applyMiddleware } from 'redux'
import RootReducer from './reducers/root.reducer'
// import thunk from 'redux-thunk'
import createSagaMiddleware from 'redux-saga'
// import counterSaga from './sagas/counter.saga'
import rootSaga from './sagas/root.saga'

const sagaMiddleware = createSagaMiddleware()

// createStore 的 store 默认值是可选参数，如果传入了中间件，那第二个参数就是中间件，第三个参数可以写 store 默认值
export const store = createStore(RootReducer, applyMiddleware(
  sagaMiddleware
))

// 启动 counterSaga
// sagaMiddleware.run(counterSaga)
sagaMiddleware.run(rootSaga)