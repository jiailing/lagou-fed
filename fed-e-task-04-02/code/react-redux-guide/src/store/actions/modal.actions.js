import { HIDEMODAL, SHOWMODAL, SHOWMODAL_ASYNC} from "../const/modal.const"

export const show = () => ({ type: SHOWMODAL })
export const hide = () => ({ type: HIDEMODAL })

// 给 thunk 使用
// export const show_async = () => dispatch => {
//   setTimeout(() => {
//     dispatch(show())
//   }, 2000);
// }

// 给 saga 使用
export const show_async = () => ({ type: SHOWMODAL_ASYNC })