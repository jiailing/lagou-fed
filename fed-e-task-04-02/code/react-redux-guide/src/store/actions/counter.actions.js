// import { DECREMENT, INCREMENT, INCREMENT_ASYNC } from "../const/counter.const"

// export const increment = (payload) => ({type: INCREMENT, payload})
// export const decrement = (payload) => ({type: DECREMENT, payload})

// 给 thunk 使用 
// export const increment_async = (payload) => dispatch => {
//   setTimeout(() => {
//     dispatch(increment(payload))
//   }, 2000);
// }

// 给 saga 使用
// export const increment_async = (payload) => ({ type: INCREMENT_ASYNC, payload});

// 使用 redux-actions 
import { createAction } from 'redux-actions'

export const increment = createAction('increment')
export const decrement = createAction('decrement')