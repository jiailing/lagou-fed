import { handleActions as createReducer } from "redux-actions";
import { addProductToLocalCart, changeLocalProductNumber, deleteProductFromLocalCart, saveCarts } from "../actions/cart.actions";

const initialState = []

// 将商品添加到本地的购物车数据中
const handleAddProductToLocalCart = (state, action) => {
  // 1. 要添加的商品没有在购物车中，直接添加
  // 2. 要添加的商品已经在购物车中，将商品数量加一

  // 将原有的商品数据拷贝一份
  const newState = JSON.parse(JSON.stringify(state))
  const product = newState.find(product => product.id === action.payload.id)
  if (product) {
    // 商品已存在
    product.count = Number(product.count) + 1
  } else {
    // 商品不存在
    newState.push(action.payload)
  }
  return newState
}

const handleSaveCarts = (state, action) => {
  return action.payload
}

const handleDeleteProductFromLocalCart = (state, action) => {
  // 将原有的商品数据拷贝一份
  const newState = JSON.parse(JSON.stringify(state))
  newState.splice(action.payload, 1);
  return newState
}

// 更新本地购物车中的数量
const handleChangeLocalProductNumber = (state, action) => {
  // 将原有的商品数据拷贝一份
  const newState = JSON.parse(JSON.stringify(state))
  const product = newState.find(product => product.id === action.payload.id)
  product.count = action.payload.count
  return newState
}

export default createReducer({
  // 将商品添加到本地的购物车数据中
  [addProductToLocalCart]: handleAddProductToLocalCart,
  // 将服务端返回的购物车列表数据同步到本地的购物车中
  [saveCarts]: handleSaveCarts,
  // 删除本地购物车中的商品
  [deleteProductFromLocalCart]: handleDeleteProductFromLocalCart,
  // 更新本地购物车中的数量
  [changeLocalProductNumber]: handleChangeLocalProductNumber
}, initialState)