import { takeEvery, put } from "redux-saga/effects";
import { loadProducts, saveProducts } from "../actions/product.actions";
import axios from "axios";

function * handleLoadProduct (params) {
  // 向服务器端发送请求，加载商品列表数据
  const { data } = yield axios.get('http://localhost:3005/goods')
  // 将商品列表数据保存到本地的 store 对象中
  yield put(saveProducts(data))
}

export default function * productSaga () {
  yield takeEvery(loadProducts, handleLoadProduct)
}