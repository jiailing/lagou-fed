import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
import * as cartActions from '../store/actions/cart.actions'

class Cart extends Component {

  componentDidMount () {
    this.props.loadCarts()
  }

  changeProductNumber = (cid, e) => {
    // 获取商品数量
    const count = e.target.value;
    // 向服务器端发送请求，告诉服务器端我们要将哪一个商品的数量更改成什么
    this.props.changeServiceProductNumber({cid, count})
  }

  render () {
    const { carts, deleteProductFromCart } = this.props
    const total = carts.reduce((total, product) => total += product.count * product.price, 0)
    return (
      <section className="container content-section">
            <h2 className="section-header">购物车</h2>
            <div className="cart-row">
                <span className="cart-item cart-header cart-column">商品</span>
                <span className="cart-price cart-header cart-column">价格</span>
                <span className="cart-quantity cart-header cart-column">数量</span>
            </div>
            <div className="cart-items">
            {
              carts.map(product => (
                <div className="cart-row" key={product.id}>
                    <div className="cart-item cart-column">
                        <img className="cart-item-image" src={`http://localhost:3005${product.thumbnail}`} width="100" height="100" alt={product.title} />
                        <span className="cart-item-title">{product.title}</span>
                    </div>
                    <span className="cart-price cart-column">￥{product.price}</span>
                    <div className="cart-quantity cart-column">
                        <input className="cart-quantity-input" type="number" min={1} value={product.count} onChange={(e) => this.changeProductNumber(product.id, e)} />
                        <button className="btn btn-danger" type="button" onClick={() => deleteProductFromCart(product.id)}>删除</button>
                    </div>
                </div>
              ))
            }
            </div>
            <div className="cart-total">
                <strong className="cart-total-title">总价</strong>
                <span className="cart-total-price">￥{total}</span>
            </div>
        </section>
    )
  }
}

const mapStateToProps = (state) => ({
  carts: state.carts
})

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(cartActions, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(Cart)