import Product from './Product'
import Cart from './Cart'


function App() {
  return (
    <div>
      <Product />
      <Cart />
    </div>
  );
}

export default App;
