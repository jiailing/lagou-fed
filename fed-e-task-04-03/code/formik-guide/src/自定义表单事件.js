import * as Yup from 'yup'
import { Formik, Form, Field, ErrorMessage, useField } from 'formik'

function MyInputField ({ label, ...props }) {
  const [field, meta] = useField(props)
  return <div>
    <label htmlFor={props.id}>{label}</label>
    <input {...field} {...props} />
    <span>{ meta.touched && meta.error ? meta.error: null }</span>
  </div>
}


export default function App () {

  const initialValues = {
    username: '张三',
    content: '我是内容',
    subject: 'java',
  }
  const handleSubmit = values => {
    console.log(values)
  }
  const schema = Yup.object({
    username: Yup.string().max(15, '用户名的长度不能大于15').required('请输入用户名'),
    password: Yup.string().min(6, '密码的长度不能小于6').required('请输入密码'),
  })
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={schema}
    >
      <Form>
        <Field name="username" />
        <ErrorMessage name="username" />
        <Field as="textarea" name="content" />
        <Field as="select" name="subject" >
          <option value="java">java</option>
          <option value="js">js</option>
        </Field>
        <MyInputField id="myPass" label="密码" type="password" name="password" placeholder="请输入密码" />
        <button type="submit">提交</button>
      </Form>
    </Formik>
  )
}