import { Formik, Form, Field, ErrorMessage } from 'formik'
import * as Yup from 'yup'

export default function App () {

  const initialValues = {
    username: '张三',
    content: '我是内容',
    subject: 'java',
  }
  const handleSubmit = values => {
    console.log(values)
  }
  const schema = Yup.object({
    username: Yup.string().max(15, '用户名的长度不能大于15').required('请输入用户名')
  })
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={schema}
    >
      <Form>
        <Field name="username" />
        <ErrorMessage name="username" />
        <Field as="textarea" name="content" />
        <Field as="select" name="subject" >
          <option value="java">java</option>
          <option value="js">js</option>
        </Field>
        <button type="submit">提交</button>
      </Form>
    </Formik>
  )
}