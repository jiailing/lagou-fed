/** @jsx jsx */
import { jsx } from '@emotion/core'

function App() {
  return (
    <div css={{width: 200, height: 200, background: 'pink'}}>App works</div>
  );
}

export default App;
