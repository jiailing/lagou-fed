const express = require('express')
const cors = require('cors')
const app = express()

app.use(cors())

const hostname = '127.0.0.1'
const port = 8081

const _products = [
  { id: 1, title: 'iPhone 8', price: 8000 },
  { id: 1, title: 'iPhone 9', price: 9000 },
  { id: 1, title: 'iPhone 10', price: 9990 },
]

app.use(express.json())

app.get('/products', (req, res) => {
  res.status(200).json(__products)
})

app.post('/checkout', (req, res) => {
  res.status(200).json({
    success: Math.random() > 0.5
  })
})
