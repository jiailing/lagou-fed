import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export const createStore = () => {
  return new Vuex.Store({
    state: () => ({
      posts: []
    }),
    mutations: {
      setPosts (state, data) {
        state.posts = data
      }
    },
    actions: {
      // 在服务端渲染期间，务必让action返回一个promise 
      async getPosts ({commit}) { // async默认返回Promise
        // return new Promise()
        const { data } = await axios.get('https://cnodejs.org/api/v1/topics')
        commit('setPosts', data.data)
      }
    }
  })
}