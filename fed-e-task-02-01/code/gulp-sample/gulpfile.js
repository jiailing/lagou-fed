const {series, parallel} = require('gulp')

// gulp的入口文件
exports.foo = done => {
  console.log('foo task working...')

  done() // 标识任务完成
}

exports.default = done => {
  console.log('default task working...')
  done()
}

const task1 = done => {
  setTimeout(() => {
    console.log('task1 working...')
    done()
  }, 1000);
}

const task2 = done => {
  setTimeout(() => {
    console.log('task2 working...')
    done()
  }, 1000);
}

const task3 = done => {
  setTimeout(() => {
    console.log('task3 working...')
    done()
  }, 1000);
}

// series 串行执行
// exports.bar = series(task1, task2, task3)

// parallel 并行执行
exports.bar = parallel(task1, task2, task3)