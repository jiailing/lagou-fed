// Grunt的入口文件
// 用于定义一些需要Grunt自动执行的任务
// 需要导出一个函数
// 此函数接受一个grunt的形参，内部提供一些创建任务时可以用到的API

module.exports = grunt => {

  grunt.initConfig({
    foo: {
      bar: 123
    },
    build: {
      options: { // 是配置选项，不会作为任务
        foo: 'bar'
      },
      css: {
        options: { // 会覆盖上层的options
          foo: 'baz'
        }
      },
      js: '2'
    }
  })

  grunt.registerTask('foo', () => {
    console.log(grunt.config('foo.bar'))
  })

  grunt.registerTask('bar', '任务描述', () => {
    console.log('other task~')
  })

  grunt.registerTask('default', () => {
    console.log('default task')
  })

  grunt.registerTask('default', ['foo', 'bad', 'bar'])

  // grunt.registerTask('async-task', () => {
  //   setTimeout(() => {
  //     console.log('async task working')
  //   }, 1000);
  // })

  // 异步任务，done()表示结束
  grunt.registerTask('async-task', function () {
    const done = this.async()
    setTimeout(() => {
      console.log('async task working..')
      done()
    }, 1000);
  })

  // 失败任务
  grunt.registerTask('bad', () => {
    console.log('bad working...')
    return false
  })

  // 异步失败任务，done(false)表示结束
  grunt.registerTask('bad-async-task', function () {
    const done = this.async()
    setTimeout(() => {
      console.log('bad async task working..')
      done(false)
    }, 1000);
  })

  // 多目标任务，可以让任务根据配置形成多个子任务
  grunt.registerMultiTask('build', function () {
    console.log(this.options())
    console.log(`build task: ${this.target}, data: ${this.data}`)
  })
  /*
    Running "build:css" (build) task
    { foo: 'baz' }
    build task: css, data: [object Object]

    Running "build:js" (build) task
    { foo: 'bar' }
    build task: js, data: 2
  */
}