// 兼容web的API
// const res = await fetch('https://api.github.com')
// const data = await res.json()
// console.log(data)

// deno run --allow-net index.ts

// 全局Deno命名空间 沙箱模式运行

// Deno中的运行时默认采用Promise读文件
const decoder = new TextDecoder('utf-8')
const buffer = await Deno.readFile('./a.txt')
const contents = decoder.decode(buffer)

console.log(contents)

// deno run --allow-read index.ts