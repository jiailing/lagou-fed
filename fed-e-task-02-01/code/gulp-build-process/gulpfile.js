const fs = require('fs')
const {Transform} = require('stream')

exports.default = () => {
  const read = fs.createReadStream('normalize.css')
  const write = fs.createWriteStream('normalize.min.css')
  // 文件转化流
  const transform = new Transform({
    transform: (chunk, encoding, callback) => {
      // 核心转化过程
      // chunk => 读取流中读取的内容(Buffer )
      const input = chunk.toString()
      // 转化空白符和注释
      const output = input.replace(/\s+/g, '').replace(/\/\*.+?\*\//g, '')
      callback(null, output)
    }
  })

  read
  .pipe(transform) // 先转化
  .pipe(write)

  return read
}