import { init, h } from 'snabbdom'
// 1. 导入需要的模块
import style from 'snabbdom/modules/style'
import eventlisteners from 'snabbdom/modules/eventlisteners'
// 2. init()中注册模块
let patch = init([
  style, eventlisteners
])
// 3. 使用h()函数创建VNode的时候，可以把第二个参数设置为对象，其他参数往后移
let vnode = h('div', {
  style: {
    backgroundColor: 'red'
  },
  on: {
    click: eventHandler
  }
}, [h('h1', 'Hello Snabbdom'), h('p', 'this is a p')])

let app = document.querySelector('#app')

patch(app, vnode)

function eventHandler () {
  console.log('click~~')
}