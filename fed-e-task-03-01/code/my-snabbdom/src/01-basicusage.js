import { h, init } from 'snabbdom'

// 1. hello world
// init方法，参数：数组，模块
// 返回值：patch函数,作用是对比两个vnode的差异更新到真实DOM
let patch = init([])

// h函数： 
// 第一个参数：标签+选择器，
// 第二个参数：如果是字符串的话，就是标签中的内容
// 返回值：vnode
let vnode = h('div#container.cls', 'Hello world')

// 要占位的DOM元素
let app = document.querySelector('#app')

// patch函数
// 第一个参数：可以是DOM元素，内部会把DOM元素转化成VNode
// 第二个参数：VNode
// 返回值：VNode
let oldVNode = patch(app, vnode)

// 假设的时刻
vnode = h('div', 'Hello Snabbdom')
patch(oldVNode, vnode)
