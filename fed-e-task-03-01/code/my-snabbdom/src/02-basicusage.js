// 2. div中放置子元素 h1, p 

import { h, init } from 'snabbdom'

let patch = init([])

let vnode = h('div#container', [h('h1', 'hello snabbdom'), h('p', '这是一个p标签')])

let app = document.querySelector('#app')

let oldVnode = patch(app, vnode)

setInterval(() => {
  vnode = h('div#container', [h('h1', 'hello world'), h('p', 'hello p')])
  patch(oldVnode, vnode)

  // 清空页面内容--官网上的写法是错误的
  // patch(oldVnode, null)
  // 这是正确的写法，第二个参数是一个注释节点
  patch(oldVnode, h('!'))
}, 2000);