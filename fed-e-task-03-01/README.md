# 计爱玲 Part3 模块一 手写 Vue Router、手写响应式实现、虚拟 DOM 和 Diff 算法

> 本次作业的视频演示中没有说话来介绍，因为前两次在视频中讲话会使录制时间太长，所以我这次把想要描述的地方都写在本文档中了


## 一、简答题

### 1、当我们点击按钮的时候动态给 data 增加的成员是否是响应式数据，如果不是的话，如果把新增成员设置成响应式数据，它的内部原理是什么。

```js
let vm = new Vue({
 el: '#el'
 data: {
  o: 'object',
  dog: {}
 },
 method: {
  clickHandler () {
   // 该 name 属性是否是响应式的
   this.dog.name = 'Trump'
  }
 }
})
```

 答：**不是**，data属性是在创建Vue实例的时候，将其转化为响应式数据。而Vue实例化之后，再给data增加成员，仅仅是增加了一个普通的js属性而已，并不是响应式的。

对于已经创建的实例，Vue 不允许动态添加根级别的响应式 property。但是，可以使用 `Vue.set(object, propertyName, value)` 方法向嵌套对象添加响应式 property。例如:

```js
Vue.set(vm.someObject, 'b', 2)
```

此外，还可以使用 `vm.$set` 实例方法，这也是全局 `Vue.set` 方法的别名：

```js
this.$set(this.someObject,'b',2)
```

**Vue.set的内部原理**：调用了`defineReactive(ob.value, key, val)`和`ob.dep.notify()`,也就是将新添加的属性设为响应式数据，在属性的set方法触发时，调用notify方法通知观察者更新页面。

### 2、请简述 Diff 算法的执行过程

 答：Diff算法是找同级别的子节点一次比较，然后再找下一级别的节点比较，算法时间复杂度为O(n)。

在进行同级别节点比较的时候，首先会对新老节点数组的开始和结尾节点设置标记索引，遍历的过程中移动索引。

在对开始和结束节点比较的时候，总共有四种情况：

+ oldStartVnode / newStartVnode (旧开始节点 / 新开始节点)
+ oldEndVnode / newEndVnode (旧结束节点 / 新结束节点)
+ oldStartVnode / newEndVnode (旧开始节点 / 新结束节点)
+ oldEndVnode / newStartVnode (旧结束节点 / 新开始节点)

开始循环判断：

首先判断oldStartVnode和newStartVnode是sameVnode (key 和 sel 相同)：

+ 调用patchVnode()对比和更新节点
+ 把旧开始和新开始索引往后移， oldStartIdx++ / newStartIdx++

否则判断oldEndVnode和newEndVnode是sameVnode (key 和 sel 相同)：

+ 调用patchVnode()对比和更新节点
+ 把旧结束和新结束索引往前移， oldEndIdx-- / oldEndIdx--

否则判断oldStartVnode和newEndVnode是sameVnode (key 和 sel 相同)：

+ 调用patchVnode()对比和更新节点
+ 把oldStartVnode对应的DOM元素，移动到右边
+ 把旧开始索引往后移，新结束索引往前移， oldStartIdx++ / newEndIdx--

否则判断oldEndVnode和newStartVnode是sameVnode (key 和 sel 相同)：

+ 调用patchVnode()对比和更新节点
+ 把oldEndVnode对应的DOM元素，移动到左边
+ 把旧结束和新开始索引往前移， oldEndIdx-- / newStartIdx++

如果不是以上四种情况：

+ 遍历新节点，使用newStartVnode的key在老节点数组中找相同节点
+ 如果没找到，说明newStartVnode是新节点
  + 创建新节点对应的DOM元素，插入到DOM树中
+ 如果找到了
  + 判断新节点和找到的老节点的sel选择器是否相同
  + 如果不相同，说明节点被修改了
    + 重新创建对应的DOM元素，插入到DOM树中
  + 如果相同，把elmToMove对应的DOM元素，移动到左边

循环结束

+ 当老节点的所有子节点先遍历完(odlStartIdx > oldEndIdx)，循环结束
+ 新节点的所有子节点先遍历完(newStartIdx > newEndIdx)，循环结束

如果老节点的数组先遍历完(oldStartIdx > oldEndIdx)，说明新节点有剩余，把剩余节点批量插入到左边

如果新节点的数组先遍历完(newStartIdx > newEndIdx)，说明老节点有剩余，把剩余节点批量删除

## 二、编程题

### 1、模拟 VueRouter 的 hash 模式的实现，实现思路和 History 模式类似，把 URL 中的 # 后面的内容作为路由的地址，可以通过 hashchange 事件监听路由地址的变化。

+ 演示地址：[hash-vue-router.mp4](./hash-vue-router.mp4)   
+ 代码地址：https://gitee.com/jiailing/lagou-fed/tree/master/fed-e-task-03-01/code/my-hash-vue-router

+ **思路说明**

  + 自定义`VueRouter`类，主要方法与课程中实现History模式路由类似
  + 大的区别是，在创建`router-link`组件的时候，由于`URL`中`#`后面的内容才是路由，所以`a`标签的`href`属性的值应该是`'/#' + this.to`
  + 此外，`a`标签不需要再自定义点击事件并禁用默认事件，`a`标签的默认点击就会修改`URL`
  + 我们需要监听的事件是`hashchange`，当`hashchange`事件被触发时，将当前的响应式数据中保存当前路由的属性`current`的值修改为`URL`的`#`号后面的内容: `this.data.current = window.location.hash.substr(1)`

+ 核心代码

  + src/router/index.js

    ```js
    import Vue from 'vue'
    import VueRouter from '../vuerouter'
    import Home from '../views/Home.vue'
    
    Vue.use(VueRouter)
    
    const routes = [
      {
        path: '/',
        name: 'Home',
        component: Home
      },
      {
        path: '/ablout',
        name: 'About',
        component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
      }
    ]
    
    const router = new VueRouter({
      mode: 'hash',
      base: process.env.BASE_URL,
      routes
    })
    
    export default router
    ```

  + src/vuerouter/index.js

    ```js
    let _Vue = null
    
    export default class VueRouter {
      static install (Vue) {
        // 1. 判断当前插件是否已经被安装
        if (VueRouter.install.installed) return
        VueRouter.install.installed = true
        // 2. 把Vue构造函数记录到全局变量
        _Vue = Vue
        // 3. 把创建Vue实例时候传入的router对象注入到Vue实例上
        // 混入
        _Vue.mixin({
          beforeCreate () {
            if (this.$options.router) {
              _Vue.prototype.$router = this.$options.router
              this.$options.router.init()
            }
          }
        })
      }
    
      constructor (options) {
        this.options = options
        this.routeMap = {}
        // _Vue.observable创建响应式对象
        this.data = _Vue.observable({
          current: '/'
        })
      }
    
      init () {
        this.createRoutMap()
        this.initComponents(_Vue)
        this.initEvent()
      }
    
      createRoutMap () {
        // 遍历所有的路由规则，把路由规则解析成键值对的形式，存储到routeMap中
        this.options.routes.forEach(route => {
          this.routeMap[route.path] = route.component
        })
      }
    
      initComponents (Vue) {
        Vue.component('router-link', {
          props: {
            to: String
          },
          render (h) {
            return h('a', {
              attrs: {
                href: '/#' + this.to
              }
            }, [this.$slots.default])
          }
          // template: '<a href="to"><slot></slot></a>'
        })
        const self = this
        Vue.component('router-view', {
          render (h) {
            const component = self.routeMap[self.data.current]
            return h(component)
          }
        })
      }
    
      initEvent () {
        window.addEventListener('hashchange', () => {
          console.log(window.location.hash)
          this.data.current = window.location.hash.substr(1)
        })
      }
    }
    
    ```

### 2、在模拟 Vue.js 响应式源码的基础上实现 v-html 指令，以及 v-on 指令。

+ 演示地址：[vue-html-on.mp4](./vue-html-on.mp4)
+ 代码地址：https://gitee.com/jiailing/lagou-fed/tree/master/fed-e-task-03-01/code/jal-vue-html-on

+ v-html

  + **实现原理**：`v-html`的原理和`v-text`相似，不同的是`v-html`是将变量赋值到元素的`innerHTML`属性上

  + compiler.js 核心代码片段：

    ```js
    // 处理v-html指令
    htmlUpdater (node, value, key) {
      node.innerHTML = value
      // 创建watcher对象
      new Watcher(this.vm, key, (newValue) => {
        node.innerHTML = newValue
      })
    }
    ```

  + index.html核心代码片段：

    ```html
    <h3 v-html="html"></h3>
    
    <script>
      let vm = new Vue({
        el: '#app',
        data: {
          html: '<span style="color: pink; font-size: 14px">I am a span</span>'
        }
      })
    </script>
    ```

+ v-on

  + **实现原理**：`v-on`事件绑定我实现了js语句块和和js函数两种形式，如：`v-on:click="alert('提交成功！')"`和`v-on:click="myFn"`两种形式，先将属性名中的事件名提取出来，再通过**正则表达式**判断属性值是否是函数名，如果不是函数名，则是第一种情况，如果是函数名，则是第二种情况。第一种使用的是`eval()`执行的js语句，第二种是将`methods`中的方法挂载到Vue实例中，然后通过调用`this.vm[script]()`

  + vue.js核心代码片段：

    ```js
    class Vue {
      constructor (options) {
        // ...
        
        // 通过属性保存选项的数据
        this.$methods = options.methods || {}
        
        // 把methods中的方法注入到Vue实例中
        this._proxyMethods(this.$methods)
    		
        // ...
      }
    
      _proxyMethods (methods) {
        // 遍历methods中的所有方法
        Object.keys(methods).forEach(key => {
          // 把methods的方法注入到vue实例中
          this[key] = methods[key]
        })
      }
    }
    ```

  + Compiler.js

    ```js
    class Compiler {
    
      // ...
      
      update (node, key, attrName) {
        if(attrName.startsWith('on')) { // 以on开头的属性，表示是事件绑定
          const event = attrName.replace('on:', '') // 获取事件名
          return this.onUpdater && this.onUpdater(node, event, key)
        }
        const updateFn = this[`${attrName}Updater`]
        updateFn && updateFn.call(this, node, this.vm[key], key)
      }
    
      // 处理v-on指令
      onUpdater (node, event, script) {
        // 判断如果是函数名的话，则去执行Vue实例上的methods上的方法：
        if(/^[^\d]\w+$/.test(script)) {
          return node.addEventListener(event, (e) => this.vm[script](e))
        }
        // 否则是js语句：
        node.addEventListener(event, function () {
          eval(script)
        })
      }
      
      // ...
    }
    ```

  + index.html 核心代码片段

    ```html
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Document</title>
    </head>
    <body>
      <div id="app">
        
        <button v-on:click="alert('提交成功！')">提交</button>
        
        <button v-on:click="myFn">测试v-on绑定函数</button>
        
      </div>
      <script src="./js/dep.js"></script>
      <script src="./js/watcher.js"></script>
      <script src="./js/observer.js"></script>
      <script src="./js/compiler.js"></script>
      <script src="./js/vue.js"></script>
      <script>
        let vm = new Vue({
          el: '#app',
          data: {
            msg: 'hello vue',
          },
          methods: {
            myFn () {
              this.msg = '修改msg了'
              alert(this.msg)
            }
          }
        })
      </script>
    </body>
    </html>
    ```

### 3、参考 Snabbdom 提供的电影列表的示例，实现类似的效果。

+ 演示地址：[snabbdom-reorder.mp4](./snabbdom-reorder.mp4)
+ 代码地址：https://gitee.com/jiailing/lagou-fed/tree/master/fed-e-task-03-01/code/my-snabbdom

+ **实现原理**

  + 先从Snabbdom中导入h函数和init函数，init函数用于初始化patch函数，h函数用于生成vnode，patch函数可以对比新旧vnode，并且更新到页面上。
  + 在页面加载完成之后，我们将初始数据传入到h函数中生成vnode，然后patch到id为container的div上。在初始化页面的时候，左上角三个a标签，
    + 通过eventlisteners模块调用传入的on属性中的点击事件，回调事件就是排序函数
    + 通过class模块判断该节点是否有选中的样式
  + 处理好表头上的三个标签后，再通过style模块设置list的高度
  + h函数在处理data数组的数据时，通过props模块给数组的每个节点传入属性key，通过style属性设置opacity和transform，以及通过delayed属性和remove属性，设置了刚加载list和移除元素时的动画效果。
  + hook属性中设置了insert属性，也就是在节点插入到dom中后，触发该回调，设置节点的高度。
  + 最后再给每个list中的节点的右上角增加删除按钮，即通过eventlisteners模块调用传入的on属性中的点击事件，点击事件的回调函数就是删除改行。
  + 添加按钮的回调函数就是随机生成一个数据对象，但是其rank属性是全局递增的。然后对新数据调用h函数生成新的vnode，并且patch到页面上，重新渲染了页面。
  + 行删除按钮就是移除该行元素，然后重新渲染页面。
  + 排序函数就是根据指定的属性对数据重新进行排序，排序后重新渲染页面。

+ **bug修复**：Snabbdom的样例中的演示list每一行元素删除的时候，当删除到最后一个元素时，会发生报错：`Uncaught TypeError: Cannot read property 'offset' of undefined`,这是因为Snabbdom中该样例的render函数中的totalHeight没有非空判断。我已经将该bug提交到Snabbdom仓库的issue里了。

+ 代码

  + package.json

    ```json
    {
      "name": "my-snabbdom",
      "version": "1.0.0",
      "main": "index.js",
      "license": "MIT",
      "scripts": {
        "dev": "parcel index.html --open"
      },
      "devDependencies": {
        "parcel-bundler": "^1.12.4"
      },
      "dependencies": {
        "snabbdom": "0.7.4"
      }
    }
    
    ```

  + index.html

    ```html
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Snabbdom</title>
      <link rel="stylesheet" href="./src/style/index.css">
    </head>
    <body>
      <div id="container"></div>
      <script src="./src/script/index.js"></script>
    </body>
    </html>
    ```

  + src/index.js

    ```js
    import { h, init } from 'snabbdom'
    import style from 'snabbdom/modules/style'
    import props from 'snabbdom/modules/props'
    import className from 'snabbdom/modules/class'
    import eventlisteners from 'snabbdom/modules/eventlisteners'
    let patch = init([
      style, props, className, eventlisteners
    ])
    
    var vnode;
    
    var nextKey = 11;
    var margin = 8;
    var sortBy = 'rank';
    var totalHeight = 0;
    var originalData = [
      {rank: 1, title: 'The Shawshank Redemption', desc: 'Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.', elmHeight: 0},
      {rank: 2, title: 'The Godfather', desc: 'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.', elmHeight: 0},
      {rank: 3, title: 'The Godfather: Part II', desc: 'The early life and career of Vito Corleone in 1920s New York is portrayed while his son, Michael, expands and tightens his grip on his crime syndicate stretching from Lake Tahoe, Nevada to pre-revolution 1958 Cuba.', elmHeight: 0},
      {rank: 4, title: 'The Dark Knight', desc: 'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, the caped crusader must come to terms with one of the greatest psychological tests of his ability to fight injustice.', elmHeight: 0},
      {rank: 5, title: 'Pulp Fiction', desc: 'The lives of two mob hit men, a boxer, a gangster\'s wife, and a pair of diner bandits intertwine in four tales of violence and redemption.', elmHeight: 0},
      {rank: 6, title: 'Schindler\'s List', desc: 'In Poland during World War II, Oskar Schindler gradually becomes concerned for his Jewish workforce after witnessing their persecution by the Nazis.', elmHeight: 0},
      {rank: 7, title: '12 Angry Men', desc: 'A dissenting juror in a murder trial slowly manages to convince the others that the case is not as obviously clear as it seemed in court.', elmHeight: 0},
      {rank: 8, title: 'The Good, the Bad and the Ugly', desc: 'A bounty hunting scam joins two men in an uneasy alliance against a third in a race to find a fortune in gold buried in a remote cemetery.', elmHeight: 0},
      {rank: 9, title: 'The Lord of the Rings: The Return of the King', desc: 'Gandalf and Aragorn lead the World of Men against Sauron\'s army to draw his gaze from Frodo and Sam as they approach Mount Doom with the One Ring.', elmHeight: 0},
      {rank: 10, title: 'Fight Club', desc: 'An insomniac office worker looking for a way to change his life crosses paths with a devil-may-care soap maker and they form an underground fight club that evolves into something much, much more...', elmHeight: 0},
    ];
    var data = [
      originalData[0],
      originalData[1],
      originalData[2],
      originalData[3],
      originalData[4],
      originalData[5],
      originalData[6],
      originalData[7],
      originalData[8],
      originalData[9],
    ];
    
    function movieView(movie) {
      return h('div.row', {
        key: movie.rank,
        style: {
          opacity: '0', 
          transform: 'translate(-200px)',
          delayed: {
            transform: `translateY(${movie.offset}px)`, opacity: '1'
          },
          remove: {
            opacity: '0',
            transform: `translateY(${movie.offset}px) translateX(200px)`
          }
        },
        hook: {insert: (vnode) => { movie.elmHeight = vnode.elm.offsetHeight; }},
      }, [
        h('div', {style: {fontWeight: 'bold'}}, movie.rank),
        h('div', movie.title),
        h('div', movie.desc),
        h('div.btn.rm-btn', {on: {click: [remove, movie]}}, 'x'),
      ]);
    }
    
    function view(data) {
      return h('div#container', [
        h('h1', 'Top 10 movies'),
        h('div', [
          h('a.btn.add', {on: {click: add}}, 'Add'),
          'Sort by: ',
          h('span.btn-group', [
            h('a.btn.rank', {class: {active: sortBy === 'rank'}, on: {click: [changeSort, 'rank']}}, 'Rank'),
            h('a.btn.title', {class: {active: sortBy === 'title'}, on: {click: [changeSort, 'title']}}, 'Title'),
            h('a.btn.desc', {class: {active: sortBy === 'desc'}, on: {click: [changeSort, 'desc']}}, 'Description'),
          ])
        ]),
        h('div.list', {style: {height: totalHeight+'px'}}, data.map(movieView))
      ])
    }
    
    function add() {
      var n = originalData[Math.floor(Math.random() * 10)];
      data = [{rank: nextKey++, title: n.title, desc: n.desc, elmHeight: 0}].concat(data);
      render();
      render();
    }
    
    function remove(movie) {
      data = data.filter((m) => { return m !== movie; });
      render();
    }
    
    function changeSort(prop) {
      sortBy = prop;
      data.sort((a, b) => {
        if (a[prop] > b[prop]) {
          return 1;
        }
        if (a[prop] < b[prop]) {
          return -1;
        }
        return 0;
      });
      render();
    }
    
    function render() {
      data = data.reduce((acc, m) => {
        var last = acc[acc.length - 1];
        m.offset = last ? last.offset + last.elmHeight + margin : margin;
        return acc.concat(m);
      }, []);
      if(data.length === 0) {
        totalHeight = 0
      } else {
        totalHeight = data[data.length - 1].offset + data[data.length - 1].elmHeight;
      }
      vnode = patch(vnode, view(data));
    }
    
    window.addEventListener('DOMContentLoaded', () => {
      const container = document.getElementById('container')
      vnode = patch(container, view(data))
      render()
    })
    ```

  + style/index.css

    ```css
    body {
      background: #fafafa;
      font-family: sans-serif;
    }
    h1 {
      font-weight: normal;
    }
    .btn {
      display: inline-block;
      cursor: pointer;
      background: #fff;
      box-shadow: 0 0 1px rgba(0, 0, 0, .2);
      padding: .5em .8em;
      transition: box-shadow .05s ease-in-out;
      -webkit-transition: box-shadow .05s ease-in-out;
    }
    .btn:hover {
      box-shadow: 0 0 2px rgba(0, 0, 0, .2);
    }
    .btn:active, .active, .active:hover {
      box-shadow: 0 0 1px rgba(0, 0, 0, .2),
                  inset 0 0 4px rgba(0, 0, 0, .1);
    }
    .add {
      float: right;
    }
    #container {
      max-width: 42em;
      margin: 0 auto 2em auto;
    }
    .list {
      position: relative;
    }
    .row {
      overflow: hidden;
      position: absolute;
      box-sizing: border-box;
      width: 100%;
      left: 0px;
      margin: .5em 0;
      padding: 1em;
      background: #fff;
      box-shadow: 0 0 1px rgba(0, 0, 0, .2);
      transition: transform .5s ease-in-out, opacity .5s ease-out, left .5s ease-in-out;
      -webkit-transition: transform .5s ease-in-out, opacity .5s ease-out, left .5s ease-in-out;
    }
    .row div {
      display: inline-block;
      vertical-align: middle;
    }
    .row > div:nth-child(1) {
      width: 5%;
    }
    .row > div:nth-child(2) {
      width: 30%;
    }
    .row > div:nth-child(3) {
      width: 65%;
    }
    .rm-btn {
      cursor: pointer;
      position: absolute;
      top: 0;
      right: 0;
      color: #C25151;
      width: 1.4em;
      height: 1.4em;
      text-align: center;
      line-height: 1.4em;
      padding: 0;
    }
    ```

