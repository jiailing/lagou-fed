# 计爱玲 Part2 模块二 模块化开发与规范化标准 作业

## 一、简答题

### 1. Webpack 的构建流程主要有哪些环节？如果可以请尽可能详尽的描述 Webpack 打包的整个过程。

答：webpack的构建流程以及详细打包过程：

`webpack` 构建时首先会根据配置文件或者命令行参数  `options` 生成 `compiler` 对象，`compiler` 可以理解为 `webpack` 编译的调度中心，是一个编译器实例，在 `compiler` 对象记录了完整的 `webpack` 环境信息，在 `webpack` 的每个进程中，`compiler` 只会生成一次。然后初始化 `webpack` 的内置插件及 `options` 配置。

初始化 `compiler` 后，根据 `options` 的 `watch` 判断是否启动了 `watch`，如果启动 `watch` 了就调用 `compiler.watch` 来监控构建文件，否则启动 `compiler.run` 来构建文件，`compiler.run` 就是我们此次编译的入口方法，代表着要开始编译了。

`make` 执行真正的编译构建过程，从入口文件开始，构建模块，直到所有模块创建结束。

`seal` 生成 `chunks`，对 `chunks` 进行一系列的优化操作，并生成要输出的代码。

`seal` 结束后，`Compilation` 实例的所有工作到此也全部结束，意味着一次构建过程已经结束。

`emit` 被触发之后，`webpack` 会遍历 `compilation.assets`, 生成所有文件，然后触发任务点 `done`，结束构建流程。

在每一个构建阶段，都安插了钩子，开发者可以在不同的阶段放入钩子函数，来执行一些自动化任务。

### 2. Loader 和 Plugin 有哪些不同？请描述一下开发 Loader 和 Plugin 的思路。

答：

`Loader`是对指定的资源进行处理，将资源文件从输入到输出之间的一个转换，将资源文件转化为js模块。

`Plugin`拥有更宽的能力范围，`Webpack`要求插件必须是一个函数或者是一个包含`apply`方法的对象。通过在生命周期的钩子中挂载函数实现扩展。插件机制的是`webpack`一个核心特性，目的是为了增强`webpack`自动化方面的能力。

开发`Loader`要专注实现资源模块的加载，从而去实现整体项目的打包。

`loader`文件`markdown-loader.js`：

```js
const marked = require('marked')

module.exports = source => {
  // console.log(source)
  // return 'console.log("hello")'
  const html = marked(source)
  console.log(html)
  // 两种导出方式：
  // return `module.exports=${JSON.stringify(html)}`
  return `export default ${JSON.stringify(html)}`
}
```

在`webpack.config.js`中如何使用：

```js
module: {
    rules: [
      {
        test: /.md$/,
        use: ['html-loader', './markdown-loader.js']
      }
    ]
  }
```

开发Plugin是为了解决除了资源加载以外的其他的一些自动化工作。

开发的插件可以是一个函数或者是一个包含`apply`方法的对象：

```js
class MyPlugin {
  apply (compiler) {
    console.log('MyPlugin 启动')
    compiler.hooks.emit.tap('MyPlugin', compilation => {
      // compilation 可以理解为此次打包的上下文
      for (const name in compilation.assets) {
        // console.log(name) // 文件名
        console.log(compilation.assets[name].source())
        if(name.endsWith('.js')) {
          const contents = compilation.assets[name].source()
          const withoutComments = contents.replace(/\/\*\*+\//g, '')
          compilation.assets[name] = {
            source: () => withoutComments,
            size: () => withoutComments.length
          }
        }
      }
    })
  }
}
```

如何使用自定义插件：

```js
plugins: [
  new MyPlugin()
]
```

## 二、编程题
### 1. 使用 Webpack 实现 Vue 项目打包任务

webpack.common.js

```js
const path = require('path');
const webpack = require('webpack');
const { VueLoaderPlugin } = require('vue-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
  entry: path.join(__dirname, 'src/main.js'),
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, 'dist'),
  },
  resolve: {
    extensions: ['.js', '.json', '.css', '.vue'],
  },
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    port: 5000,
    hot: true,
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: ['vue-style-loader', 'css-loader'],
      },
      {
        test: /\.less$/,
        use: ['style-loader', 'css-loader', 'less-loader'],
      },
      {
        test: /.png$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10 * 1024, // 单位是字节 10KB
            esModule: false,
          },
        },
      },
    ],
  },
  plugins: [
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      title: 'Jal-Vue-Webpack',
      template: './public/index.html',
      filename: 'index.html',
    }),
    new webpack.DefinePlugin({
      BASE_URL: JSON.stringify('/'),
    }),
  ],
};
```

Webpack.dev.js

```js
const common = require('./webpack.common');
const merge = require('webpack-merge');

module.exports = merge(common, {
  mode: 'development',
  optimization: {
    usedExports: true,
    minimize: true,
  },
  devtool: 'eval-cheap-module-source-map',
});
```

Webpack.prod.js

```js
const path = require('path');
const merge = require('webpack-merge');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const common = require('./webpack.common');

module.exports = merge(common, {
  mode: 'production',
  output: {
    filename: '[name]-[contenthash:8].bundle.js',
    path: path.join(__dirname, 'dist'),
  },
  devtool: 'none',
  plugins: [
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin({
      patterns: ['public'],
    }),
  ],
});
```

