module.exports = {
  env: {
    // 运行的环境，决定了有哪些默认全局变量
    browser: true,
    es2020: true
  },
  // eslint 继承的共享配置
  extends: [
    'standard'
  ],
  // 设置语法解析器，控制是否允许使用某个版本的语法
  parserOptions: {
    ecmaVersion: 11
  },
  // 控制某个校验规则的开启和关闭
  rules: {
    'no-alert': 'error'
  }
}
