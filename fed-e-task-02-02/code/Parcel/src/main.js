// import $ from 'jquery' // 自动安装依赖
import foo from './foo'
import './style.css'
import img from './1.png'
foo.bar()

// 动态导入，自动拆分模块
import('jquery').then($=>{
  $(document.body).append('<h1>Hello</h1>')
  $(document.body).append(`<img src="${img}" />`)
})

if(module.hot) {
  module.hot.accept(() => {
    console.log('hmr') // 模块热替换
  })
}