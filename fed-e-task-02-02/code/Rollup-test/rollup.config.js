import json from 'rollup-plugin-json'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
export default {
  // input: './src/index.js',
  // input: ['src/index.js', 'src/album.js'], // 多入口打包
  input: { // 这种写法也可以进行多入口打包
    foo: 'src/index.js',
    bar: 'src/album.js'
  },
  output: {
    // file: 'dist/bundle.js',
    // format: 'iife',
    dir: 'dist', // 动态导入时会分包成多文件
    format: 'amd' // 动态导入不支持iife
  },
  plugins: [
    json(),
    resolve(),
    commonjs()
  ]
}