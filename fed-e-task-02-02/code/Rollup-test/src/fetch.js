export default endpoint => {
  return fetch(`https://jsonplaceholder.tyicode.com${endpoint}`)
  .then(response => response.json())
}