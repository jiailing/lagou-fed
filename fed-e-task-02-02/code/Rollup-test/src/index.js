// import _ from 'lodash-es' // lodash模块的ESM版本
// import {log} from './logger'
// import messages from './message'
// import {name, version} from '../package.json'
// import cjs from './cjs.module'
// const msg = messages.hi

// log(msg)
// log(name)
// log(version)
// log(_.camelCase('hello world'))

// log(cjs)

// import('./logger').then(({ log }) => {
//   log('code splitting~')
// })

import fetchApi from './fetch'
import {log} from './logger'

fetchApi('/posts').then(data => {
  data.forEach(item => {
    log(item)
  })
})