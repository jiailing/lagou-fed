define(['./logger-88edc00c'], function (logger) { 'use strict';

  logger.fetchApi('/posts?albumId=1').then(data => {
    data.forEach(item => {
      logger.log(item);
    });
  });

});
