define(['./logger-88edc00c'], function (logger) { 'use strict';

  // import _ from 'lodash-es' // lodash模块的ESM版本

  logger.fetchApi('/posts').then(data => {
    data.forEach(item => {
      logger.log(item);
    });
  });

});
