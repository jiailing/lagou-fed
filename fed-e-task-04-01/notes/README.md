# React 设计原理解密及核心源码解读

## 一、[React基础回顾](./React基础回顾/README.md)
## 二、[Virtual DOM 及 Diff 算法](./VirtualDOM及Diff算法/README.md)
## 三、[Fiber](./Fiber/README.md)
## 四、[React核心源码解读](./React核心源码解读/README.md)

```sh
git clone --branch v16.13.1 --depth=1 https://github.com/facebook/react.git src/react
```