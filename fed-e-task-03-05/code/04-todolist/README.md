# TodoList
这是一个Vue3的项目，todolist，如下：
![image-20200919132619658](../../images/3.png)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

