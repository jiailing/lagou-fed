function debounce(fn, delay = 500) {
  return function (...args) {
    clearTimeout(fn.timer)
    fn.timer = setTimeout(() => {
      fn.apply(this, args)
    }, delay);
  }
}

function throttle (fn, delay) {
  return function (...args) {
    if (fn.timer) return
    fn.timer = setTimeout(() => {
      fn.apply(this, args)
      fn.timer = null
    }, delay);
  }
}