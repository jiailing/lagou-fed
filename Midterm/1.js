const man = {
  name: 'jscoder',
  age: 22
}
const proxy = new Proxy(man, {
  get(target, key) {
    if(target.hasOwnProperty(key)) {
      return target[key]
    }
    return new Error(`Property ${key} does not exist.`)
  }
})
console.log(proxy.name)
console.log(proxy.age)
console.log(proxy.location)