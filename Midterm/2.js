function red() {
  console.log('red')
}
function green() {
  console.log('green')
}
function yellow() {
  console.log('yellow')
}

const run = (light, time) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      light()
      resolve()
    }, time * 1000);
  })
}

function main() {
  run(red, 3)
  .then(() => {
    return run(green, 1)
  })
  .then(() => {
    return run(yellow, 2)
  })
  .then(() => {
    main()
  })
}

main()

