##### 如何安装并使用flow
1. 先执行yarn init -y
2. 执行yarn add flow-bin
3. 在代码中第一行添加flow注释： // @flow
4. 在函数中形参后面加上冒号和类型：function sum (a: number, b: number)
5. 执行yarn flow init 创建.flowconfig
6. 执行yarn flow

##### 如何移除注解

+ yarn add flow-remove-types --dev
+ yarn flow-remove-types . -d dist

使用babel配合flow转换的插件
yarn add @babel/core @babel/cli @babel/preset-flow --dev
