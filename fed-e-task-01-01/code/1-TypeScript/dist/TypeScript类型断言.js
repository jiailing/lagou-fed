"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var nums = [110, 120, 119, 112];
var res = nums.find(function (i) { return i > 0; });
// const res: number | undefined
// const square = res * res
var num1 = res; // 断言 res 是number
var square = num1 * num1;
var num2 = res; // JSX下不能使用
//# sourceMappingURL=TypeScript类型断言.js.map