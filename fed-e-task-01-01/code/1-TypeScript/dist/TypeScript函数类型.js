"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// 获取不确定参数
// function func1 (a: number, b: number): string {
// function func1 (a: number, b?: number): string {
// function func1 (a: number, b: number = 10): string {
function func1(a, b) {
    if (b === void 0) { b = 10; }
    var rest = [];
    for (var _i = 2; _i < arguments.length; _i++) {
        rest[_i - 2] = arguments[_i];
    }
    return 'func1';
}
func1(100, 200);
func1(100);
func1(100, 200, 300);
// 指定函数的形式
var func2 = function (a, b) {
    return 'f';
};
//# sourceMappingURL=TypeScript函数类型.js.map