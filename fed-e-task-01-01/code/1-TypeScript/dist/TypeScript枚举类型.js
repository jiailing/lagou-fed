"use strict";
// JS中没有枚举类型，则使用对象模拟枚举类型
// const PostStatus = {
//   Draft: 0,
//   Uppublished: 1,
//   Published: 2
// }
var post = {
    title: 'Hello TypeScript',
    content: 'Type...',
    status: 0 /* Draft */
};
//# sourceMappingURL=TypeScript枚举类型.js.map