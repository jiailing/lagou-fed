"use strict";
// TypeScript 可以完全按照JavaScript 标准语法编码
var hello2 = function (name) {
    console.log("hello, " + name);
};
hello2('TypeScript');
// hello(111)
//# sourceMappingURL=myts.js.map