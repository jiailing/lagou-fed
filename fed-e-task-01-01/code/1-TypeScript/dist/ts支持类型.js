"use strict";
var a = 'foobar';
var b = 100; // NaN Infinity
var c = true; // false
// const d: boolean = null // 严格模式下不支持赋值null
var e = undefined; // 函数没有返回值时的返回值类型
var f = null;
var g = undefined;
var h = Symbol();
// const error: string = 100
//# sourceMappingURL=ts支持类型.js.map