"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function stringify(value) {
    return JSON.stringify(value);
}
stringify('string');
stringify(100);
stringify(true);
var foo = 'string';
foo = 100;
foo.bar();
//# sourceMappingURL=TypeScript任意类型.js.map