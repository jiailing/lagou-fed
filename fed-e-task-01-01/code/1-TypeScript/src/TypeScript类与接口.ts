export {}

// 尽可能让接口简单。一个接口只约束一个能力，一个类实现多个接口
interface Eat {
  eat (foo: string): void
}
interface Run {
  run (distance: number): void
}
class  Person implements Eat, Run {
  eat(food: string): void {
    console.log(`优雅的进餐：${food}`)
  }
  run(distance: number): void {
    console.log(`直立行走：${distance}`)
  }
}

class  Animal implements Eat, Run {
  eat(food: string): void {
    console.log(`呼噜呼噜的吃：${food}`)
  }
  run(distance: number): void {
    console.log(`爬行：${distance}`)
  }
}