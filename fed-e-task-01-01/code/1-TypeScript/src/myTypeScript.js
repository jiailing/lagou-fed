// TypeScript 可以完全按照JavaScript 标准语法编码
var hello = function (name) {
    console.log("hello, " + name);
};
hello('TypeScript');
// hello(111)
