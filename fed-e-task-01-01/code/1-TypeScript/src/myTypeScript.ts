// TypeScript 可以完全按照JavaScript 标准语法编码
const hello = (name: any) => {
  console.log(`hello, ${name}`)
}

hello('TypeScript')
// hello(111)