// TypeScript 可以完全按照JavaScript 标准语法编码
const hello2 = (name: any) => {
  console.log(`hello, ${name}`)
}

hello2('TypeScript')
// hello(111)