export {}

let age = 18 // ts推断出类型是number
// age = 'str' // 会报错 不能将类型“"str"”分配给类型“number”。

let foo // 此时无法推断具体类型，foo则是动态类型，any类型

foo = 1 // 不会报错
foo = 'string' // 不会报错