function createNumberArray(length: number, value: number): number[] {
  const arr = Array<number>(length).fill(value)
  return arr
}

const res = createNumberArray(3, 100 ) // [100, 100, 100]

function createArray<T> (length: Number, value: T): T[] {
  const arr = Array<T>(length).fill(value)
}

const arrRes = createArray<string>(3, 'foo')