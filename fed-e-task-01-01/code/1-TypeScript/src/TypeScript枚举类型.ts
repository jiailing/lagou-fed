// JS中没有枚举类型，则使用对象模拟枚举类型
// const PostStatus = {
//   Draft: 0,
//   Uppublished: 1,
//   Published: 2
// }

// 枚举类型。使用时和对象属性一样
// 如果不指定值，则从0开始累加。如果制定了第一个成员的值，后面的成员则再第一个成员基础上累加。值如果是字符串，就得指定具体的值
const enum PostStatus {
  Draft = 0, 
  Uppublished = 1,
  Published = 2
}

const post = {
  title: 'Hello TypeScript',
  content: 'Type...',
  status: PostStatus.Draft
}