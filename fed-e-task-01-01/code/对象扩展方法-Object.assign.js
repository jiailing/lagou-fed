// Object.assign 用第二个参数的对象属性覆盖第一个参数的对象。返回结果为第一个对象
const source1 = {
  a: 123,
  b: 456
}
const source2 = {
  a: 333,
  c: 33
}
const target = {
  a: 11,
  b: 22
}

const result = Object.assign(target, source1, source2)
console.log(result) // { a: 333, b: 456, c: 33 }
console.log(result === target) // true

function fun(obj) {
  // obj.name = 'function.obj'
  // console.log(obj)
  const funObj = Object.assign({}, obj)
  funObj.name = 'function.obj'
  console.log(funObj)
}

const obj = {
  name: 'global obj'
}

fun(obj)
console.log(obj)