const arr = ['foo', 1, false, NaN]
// 以前使用indexOf, 存在则返回下标，不存在则返回-1， 缺点是无法判断NaN
console.log(arr.indexOf('foo')) // 0
console.log(arr.indexOf(1)) // 1
console.log(arr.indexOf(false)) // 2
console.log(arr.indexOf(NaN)) // -1

console.log(arr.includes('foo')) // true
console.log(arr.includes(1)) // true
console.log(arr.includes(false)) // true
console.log(arr.includes(NaN)) // true

// 指数运算符 **
console.log(Math.pow(2, -52)) // 2.220446049250313e-16
console.log(2 ** -52) // 2.220446049250313e-16