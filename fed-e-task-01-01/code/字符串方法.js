const message = 'Error: foo is not undefined.'

console.log(
  message.startsWith('Error'),
  message.endsWith('undefined.'),
  message.includes('foo')
)
 // true true true