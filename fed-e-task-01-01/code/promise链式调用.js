function ajax(url) {
  return new Promise(function(resolve, reject) {
    // foo()
    var xhr = new XMLHttpRequest()
    xhr.open("GET", url)
    xhr.responseType = 'json'
    xhr.onload = function () {
      if(this.status === 200)
        resolve(this.response)
      else {
        reject(new Error(this.statusText))
      }
    }
    xhr.send()
  })
}

var promise = ajax('/api/users.json')

// then 方法返回一个全新的promise对象
var promise2 = promise.then(function (res) {
  console.log(res)
}, function (error) {
  console.log(error)
})

console.log(promise2 === promise) // false

// 每一个then方法都是在为上一个then方法添加状态明确过后的回调
ajax('/api/users.json')
.then(function (value) {
  console.log(111)
  return ajax('/api/users2.json')
}, function onRejected (e){
  console.log('reject', e)
}) // => Promise
// .then(function (value) {
//   console.log('yi', value)
//   console.log(222)
//   return 'foo'
// }) // => Promise
// .then(function (value) {
//   console.log(333)
//   console.log('jal', value)
// }) // => Promise
// .then(function (value) {
//   console.log(444)
//   console.log('ji', value)
// }).catch(function onRejected(error) {
//   console.log('onRejected', error)
// })

// .catch(function onRejected(error) {
//   console.log('onRejected', error)
// })
// 就相当于
// .then(undefined, function (value) {
//   console.log(444)
//   console.log('ji', value)
// })

ajax('/api/users.json')
.then(function (value) {
  console.log(111)
  return ajax('/api/users2.json')
}).catch(function onRejected (e){
  console.log('reject', e)
})

window.addEventListener('unhandledrejection', event => {
  const {reason, promise} = event
  console.log(reason, promise)
  // reason => Promise 失败原因，一般是一个错误对象
  // promise => 出现异常的Promise对象
  event.preventDefault()
}, false)

process.on('unhandledRejection', (reason, promise) => {
  const {reason, promise} = event
  console.log(reason, promise)
  // reason => Promise 失败原因，一般是一个错误对象
  // promise => 出现异常的Promise对象
})