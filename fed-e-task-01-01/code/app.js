

function reactive(obj) {
  return new Proxy(obj, {
    get (target, property) {
      return target[property]
    },
    set (target, property, value) {
      if(target[property] !== value){
        watch(property, value)
      }
    }
  })
}

function watch(property, value) {
  console.log(`${property} changed: ${value}`)
}

const state = reactive({
  foo: 100,
  bar: 200
})

state.foo++
state.bar++