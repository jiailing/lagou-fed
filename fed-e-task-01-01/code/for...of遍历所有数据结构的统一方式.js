// for ... of 循环, 可以使用break
const arr = [1, 2, 3, 4]
for (const item of arr) { // item为每个对象实例
  console.log(item)
}

// arr.forEach(item => {
//   console.log(item)
// })

for(const item of arr) {
  console.log(item) 
  if(item > 1)break
}

// 相当于arr.forEach ,但是这个方法不能终止遍历
// arr.forEach() 不能跳出循环
// 为了终止遍历，我们之前，我们曾使用
// arr.some() 返回true
// arr.every() 返回false

const s = new Set(['foo', 'bar'])
for(const item of s) {
  console.log(item)
}
// foo bar

const m = new Map()
m.set('foo', '123')
m.set('bar', '34')

for(const item of m) {
  console.log(item)
}
// [ 'foo', '123' ]  [ 'bar', '34' ]

// 解构键和值
for(const [key, value] of m) {
  console.log(key,value)
}
// foo 123
// bar 34

const obj = {name: 'jal', age: 22}

for(const item of obj) {
  console.log(item) // TypeError: obj is not iterable
}