const arr = [1, 2, 3]
// const b = arr[1]
// const c = arr[2]
// console.log(a, b ,c) // 1 2 3
// const a = arr[0]

// const [a, b, c] = arr
// console.log(a, b ,c) // 1 2 3

// const [, , c] = arr
// console.log(c) // c

// const [a, ...c] = arr // 三个点解构只能用于最后一个位置
// console.log(c) // [ 2, 3 ]

// const [a] = arr
// console.log(a) // 1

// const [a, b, c, d] = arr
// console.log(d) // undefined

// const [a, b, c = 123, d = 'defaultValue'] = arr
// console.log(c, d) // 3 defaultValue

// const path = 'a/b'
// const [, b] = path.split('/')
// console.log(b) // b

