const MyPromise = require('./MyPromise')

let p = new MyPromise((resolve, reject) => {
  // resolve('成功')
  // throw new Error('executor error')
  // setTimeout(() => {
  // // reject('失败')
  //   resolve('成功')
  // }, 2000);
  reject('失败')
})
// p.then(value => {
//   console.log(1)
// }, error => {
//   console.log(error)
// })

function other () {
  return new MyPromise((resolve, reject) => {
    setTimeout(() => {
      resolve('other')
    }, 2000);
  })
}

// const p2 = p.then(value => {
//   console.log(value)
//   return 100
//   // return p2
//   // throw new Error('then error')
//   // return other()
// }, error => {
//   console.log(11)
//   return 10000
// })
// p2.then(value => {
//   console.log('jal then: ')
//   console.log(value)
// }, error => {
//   console.log(111)
//   console.log(error)
// })

// p.then()
// .then()
// .then(value => {
//   console.log('无参的then')
//   console.log(value)
// })

// MyPromise.all(['a', 'b', other(), 'c'])
// .then(result => {
//   console.log(result)
// }, reason => {
//   console.log(reason)
// })

// MyPromise.resolve(5)
// .then(value => {
//   console.log(value)
// })

// MyPromise.resolve(other())
// .then(value => {
//   console.log(value)
// })

// p.finally(() => {
//   console.log('finally')
//   return other()
// }).then(value => {
//   console.log(value)
// }, reason => {
//   console.log(reason)
// })

p.then((value) => {
  console.log(value)
}).catch(reason => {
  console.log('cath...')
  console.log(reason)
})