// Map 映射任意类型之间的关系. Map可以用任意对象作为键，而对象只能用字符串作为键

const obj = {}

obj[true] = 'value'
obj[1] = '11'
obj[{a: 1}] = '33'
console.log(Object.keys(obj)) // [ '1', 'true', '[object Object]' ]

const m = new Map()

const tom = {name: 'tom'}

m.set(tom, 90)
console.log(m) // Map(1) { { name: 'tom' } => 90 }
console.log(m.get(tom)) // 90