const bar = 111
const obj = {
  foo: 123,
  // bar: bar,
  bar, // 同上一行效果
  // method1: function () {
  //   console.log(`method1: ${this}`)
  // },
  method2 () {
    // 直接写一个方法，同上面的冒号属性
    console.log(`method2: ${this}`)
  },
  [Math.random()]: 123, // 计算属性名

}
console.log(obj) // { foo: 123, bar: 111, method2: [Function: method2], '0.13076137144987743': 123 }
