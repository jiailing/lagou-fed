// const str = 'my name is jiailing, \n I like wangyibo '
// console.log(str)

// my name is jiailing, 
// I like wangyibo 

// const name = 'yibo'
// const str = `this is ${name}`
// console.log(str)

// 模板字符串标签函数
const str = console.log`hello world` // [ 'hello world' ]

const name = 'tom'
const gender = true
function myTagFunc (str, name, gender) {
  console.log(str, name, gender)  // [ 'hey, ', ' is a ', '.' ] tom true
  return str[0] + name + str[1] + gender + str[2]
}

const result = myTagFunc`hey, ${name} is a ${gender}.`
console.log(result) // hey, tom is a true.