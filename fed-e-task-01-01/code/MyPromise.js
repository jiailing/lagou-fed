const PENDING = 'pending' // 等待
const FULFILLED = 'fulfilled' // 成功
const REJECTED = 'rejected' // 失败

class MyPromise {
  constructor(executor) {
    try {
      executor(this.resolve, this.reject)
    } catch (e) {
      this.reject(e)
    }
  }

  // Promise状态
  status = PENDING

  // 成功之后的值
  value = undefined

  // 失败之后的原因
  reason = undefined

  // 成功回调
  successCallback = []

  // 失败回调
  failCallback = []

  resolve = value => {
    // 如果状态不是等待，阻止程序向下执行
    if(this.status !== PENDING)return
    // 将状态改为成功
    this.status = FULFILLED
    // 保存成功之后的值
    this.value = value
    // 判断成功回调是否存在，如果存在，则调用成功回调
    // this.successCallback && this.successCallback(value)
    while(this.successCallback.length !== 0) this.successCallback.shift()()
  }

  reject = reason => {
    // 如果状态不是等待，阻止程序向下执行
    if(this.status !== PENDING)return
    // 将状态改为失败
    this.status = REJECTED
    // 保存失败之后的原因
    this.reason = reason
    // 判断失败回调是否存在，如果存在，则调用失败回调
    // this.failCallback && this.failCallback(reason)
    while(this.failCallback.length !== 0) this.failCallback.shift()()
  }

  then (successCallback, failCallback) {
    successCallback = successCallback ? successCallback: value => value
    failCallback = failCallback ? failCallback: reason => reason
    let promise2 = new MyPromise((resolve, reject) => {
      // 判断状态
      if(this.status === FULFILLED) {
        // 此处使用setTimeout是为了变成异步代码，才会有promise2，同步的话promise2还没生成。
        setTimeout(() => {
          try {
            let x = successCallback(this.value)
            // 判断x的值是普通值还是promise对象
            // 如果是普通值，直接调用resolve
            // 如果是promise对象，查看promise对象返回的结果
            // 再根据promise对象返回的结果，决定调用resolve还是调用reject
            resolvePromise(promise2, x, resolve, reject)
          } catch(e) {
            reject(e)
          }
        }, 0)
      }
      else if(this.status === REJECTED) {
        setTimeout(() => {
          try {
            let x = failCallback(this.reason)
            // 判断x的值是普通值还是promise对象
            // 如果是普通值，直接调用resolve
            // 如果是promise对象，查看promise对象返回的结果
            // 再根据promise对象返回的结果，决定调用resolve还是调用reject
            resolvePromise(promise2, x, resolve, reject)
          } catch(e) {
            reject(e)
          }
        }, 0)
      }
      else {
        this.successCallback.push(() => {
          setTimeout(() => {
            try {
              let x = successCallback(this.value)
              // 判断x的值是普通值还是promise对象
              // 如果是普通值，直接调用resolve
              // 如果是promise对象，查看promise对象返回的结果
              // 再根据promise对象返回的结果，决定调用resolve还是调用reject
              resolvePromise(promise2, x, resolve, reject)
            } catch(e) {
              reject(e)
            }
          }, 0)
        })
        this.failCallback.push(() => {
          setTimeout(() => {
            try {
              let x = failCallback(this.reason)
              // 判断x的值是普通值还是promise对象
              // 如果是普通值，直接调用resolve
              // 如果是promise对象，查看promise对象返回的结果
              // 再根据promise对象返回的结果，决定调用resolve还是调用reject
              resolvePromise(promise2, x, resolve, reject)
            } catch(e) {
              reject(e)
            }
          }, 0)
        })
      }
    })
    return promise2
  }

  catch (failCallback) {
    return this.then(undefined, failCallback)
  }

  finally (callback) {
    return this.then(value => {
      return MyPromise.resolve(callback()).then(() => value)
    }, reason => {
      return MyPromise.resolve(callback()).then(() => {throw reason})
    })
  }

  static all (arr) {
    let result = []
    let index = 0
    return new MyPromise((resolve, reject) => {
      function addData (key, value) {
        result[key] = value
        index++
        if(index === arr.length) {
          resolve(result)
        }
      }
      for (let i = 0; i < arr.length; i++) {
        let current = arr[i]
        if(current instanceof MyPromise) {
          // promise对象
          current.then(value => addData(i, value), (reason) => reject(reason))
        } else {
          // 普通值
          addData(i, arr[i])
        }
      }
    })
  }

  static resolve (p) {
    if(p instanceof MyPromise) return p
    return new MyPromise(resolve => resolve(p))
  }

}

function resolvePromise(promise2, x, resolve, reject) {
  if(promise2 === x) {
    return reject(new TypeError('Chaining cycle detected for promise #<MyPromise>'))
  }
  if( x instanceof MyPromise) {
    // promise 对象
    x.then(resolve, reject)
  } else {
    // 普通值
    resolve(x)
  }
}

module.exports = MyPromise