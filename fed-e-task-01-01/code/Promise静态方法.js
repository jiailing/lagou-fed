function ajax(url) {
  return new Promise(function(resolve, reject) {
    var xhr = new XMLHttpRequest()
    xhr.open("GET", url)
    xhr.responseType = 'json'
    xhr.onload = function () {
      if(this.status === 200)
        resolve(this.response)
      else {
        reject(new Error(this.statusText))
      }
    }
    xhr.send()
  })
}

Promise.resolve('foo')
.then(function (value) {
  console.log(value)
})

// Promise.resolve('foo') 等价于
new Promise(function (resolve, reject){
  resolve('foo')
})

var promise = ajax('/api/users.json')
var promise2 = Promise.resolve(promise)
console.log(promise === promise2) // true

Promise.resolve({
  then: function (onFulfilled, onRejected) {
    onFulfilled('foo')
  }
}).then(function (value){
  console.log(value) // foo
})

Promise.reject('anything')
.catch(function (err) {
  console.log(err) // anything
})