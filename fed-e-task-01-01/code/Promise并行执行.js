function ajax(url) {
  return new Promise(function(resolve, reject) {
    var xhr = new XMLHttpRequest()
    xhr.open("GET", url)
    xhr.responseType = 'json'
    xhr.onload = function () {
      if(this.status === 200)
        resolve(this.response)
      else {
        reject(new Error(this.statusText))
      }
    }
    xhr.send()
  })
}

var promise = Promise.all([
  ajax('/api/users.json'),
  ajax('/api/posts.json'),
])

// 只有promise里面的每一个任务都执行成功了才进入resolve
// 其中任何一个失败了，都会进入catch
promise.then(function (values) {
  console.log(values) // 返回一个数组
//   Array(2)
// 0:
// username: "yibo"
// __proto__: Object
// 1:
// name: "jiailing"
}).catch(function(err){
  console.log(err)
})

ajax('/api/urls.json')
.then( value => {
  const urls = Object.values(value)
  const tasks = urls.map(url => ajax(url))
  return Promise.all(tasks)
})
.then(values => {
  console.log(values)
})

const request = ajax('/api/posts.json')
const timeout = new Promise((resolve, reject) => {
  setTimeout(() => {
    reject(new Error('timeout'))
  }, 500);
})
Promise.race([
  request, timeout
])
.then(value=>{
  console.log(value)
})
.catch(error=>{
  console.log(error)
})
// 实现ajax请求超时控制的一种方式