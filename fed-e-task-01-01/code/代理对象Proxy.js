// Proxy 
const person = {
  name: 'jal',
  age: 20
}

const personProxy = new Proxy(person, {
  get (target, property) {
    return property in target ? target[property]: 'default'
    // console.log(target, property) // { name: 'jal', age: 20 } name
    // return 100
  },
  set (target, property, value) {
    if(property === 'age') {
      if(!Number.isInteger(value)) {
        throw new TypeError(`${value} is not an int`)
      }
    }
    console.log(target, property, value) // { name: 'jal', age: 20 } gender true
  }
})

personProxy.gender = true
// personProxy.age = '11' // TypeError: 11 is not an int

personProxy.age = 11

// console.log(personProxy.name) // 100
console.log(personProxy.name) // jal
console.log(personProxy.xxx) // default

// const personProxy = new Proxy(person, {
//   deleteProperty(target, property) {
//     console.log('delete', property) // delete age
//     delete target[property]
//   }
// })

// delete personProxy.age

const list = []

const listProxy = new Proxy(list, {
  set(target, property, value) {
    console.log('set', property, value)
    target[property] = value
    return true // 表示设置成功
  }
})

listProxy.push(100)

// set 0 100
// set length 1